/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.matrix.identity.client.unstable.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.stream.Materializer
import org.mascarene.matrix.identity.model.key.{IsValidResponse, PubKeyResponse}
import io.circe.generic.auto._

import scala.concurrent.{ExecutionContextExecutor, Future}

trait KeyApi { self: Transport =>
  implicit def system: ActorSystem
  implicit def materializer: Materializer
  implicit def executionContext: ExecutionContextExecutor

  protected def apiRoot: Uri

  def getPublicKey(keyId: String): Future[PubKeyResponse] =
    doGet[PubKeyResponse](apiRoot.withPath(apiRoot.path + s"/api/v1/pubkey/$keyId")).map(_.entity)

  def isPublicKeyValid(keyId: String): Future[IsValidResponse] =
    doGet[IsValidResponse](
      apiRoot.withPath(apiRoot.path + "/api/v1/pubkey/isvalid"),
      Seq.empty,
      Map("public_key" -> keyId)
    ).map(_.entity)

  def isEphemeralKeyValid(keyId: String): Future[IsValidResponse] =
    doGet[IsValidResponse](
      apiRoot.withPath(apiRoot.path + "/api/v1/pubkey/ephemeral/isvalid"),
      Seq.empty,
      Map("public_key" -> keyId)
    ).map(_.entity)
}
