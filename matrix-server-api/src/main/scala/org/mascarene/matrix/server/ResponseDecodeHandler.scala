/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.matrix.server

import com.typesafe.scalalogging.LazyLogging
import io.circe.Decoder
import io.circe.generic.auto._
import io.circe.parser.decode
import org.mascarene.sdk.matrix.core.ApiFailure

trait ResponseDecodeHandler extends LazyLogging {

  /**
    * Decode a server response, either to an object containing the server response (Right)
    * or to an object containing the server error response (Left)
    * @param entity
    * @param decoder
    * @tparam R
    * @return
    */
  protected def responseDecodeHandler[R](entity: String)(implicit decoder: Decoder[R]): R = {
    import cats.syntax.show._
    decode[R](entity) match {
      case Left(error) =>
        logger.debug(s"response can't be decoded to entity : '${error.show}'")
        throw new ApiFailure(
          "Failed to decode server response",
          "ORG.MASCARENE.DECODE_ERROR",
          Some(s"$error for entity: $entity")
        )
      case Right(t) => t
    }
  }

}
