package org.mascarene.utils

import com.typesafe.scalalogging.Logger

object LogUtils {
  def time[R](logger: Logger, message: String = "")(block: => R): R = {
    val t0     = System.currentTimeMillis()
    val result = block // call-by-name
    val t1     = System.currentTimeMillis()
    logger.debug(s"$message elapsed time: ${(t1 - t0)}  ms")
    result
  }
}
