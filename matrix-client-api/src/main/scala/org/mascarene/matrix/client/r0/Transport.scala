package org.mascarene.matrix.client.r0

import akka.http.scaladsl.model.{HttpHeader, Uri}
import io.circe.{Decoder, Encoder}
import org.mascarene.matrix.client.r0.model.auth.AuthToken
import org.mascarene.sdk.matrix.core.HttpApiResponse

import scala.concurrent.Future

trait Transport {
  protected def doDelete[R](
      uri: Uri,
      additionalHeaders: Seq[HttpHeader] = Seq.empty,
      queries: Map[String, String] = Map.empty
  )(implicit
      decoder: Decoder[R],
      token: Option[AuthToken] = None
  ): Future[HttpApiResponse[R]]
  protected def doGet[R](
      uri: Uri,
      additionalHeaders: Seq[HttpHeader] = Seq.empty,
      queries: Map[String, String] = Map.empty
  )(implicit
      decoder: Decoder[R],
      token: Option[AuthToken] = None
  ): Future[HttpApiResponse[R]]
  protected def doPost[Q, R](
      uri: Uri,
      request: Q,
      additionalHeaders: Seq[HttpHeader] = Seq.empty,
      queries: Map[String, String] = Map.empty
  )(implicit
      encoder: Encoder[Q],
      decoder: Decoder[R],
      token: Option[AuthToken] = None
  ): Future[HttpApiResponse[R]]
  protected def doPut[Q, R](
      uri: Uri,
      request: Q,
      additionalHeaders: Seq[HttpHeader] = Seq.empty,
      queries: Map[String, String] = Map.empty
  )(implicit
      encoder: Encoder[Q],
      decoder: Decoder[R],
      token: Option[AuthToken] = None
  ): Future[HttpApiResponse[R]]
}
