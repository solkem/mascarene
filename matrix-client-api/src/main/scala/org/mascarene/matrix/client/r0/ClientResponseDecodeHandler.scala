/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.matrix.client.r0

import com.typesafe.scalalogging.LazyLogging
import io.circe.Decoder
import io.circe.parser.decode
import io.circe.generic.auto._
import io.circe.syntax._
import io.circe.parser._
import org.mascarene.matrix.client.r0.model.auth.AuthFlow
import org.mascarene.sdk.matrix.core.{ApiError, ApiFailure}

trait ClientResponseDecodeHandler extends LazyLogging {

  /**
    * Decode a server response, either to an object containing the server response (Right)
    * or to an object containing the server error response (Left)
    * @param entity
    * @param decoder
    * @tparam R
    * @return
    */
  protected def responseDecodeHandler[R](entity: String)(implicit decoder: Decoder[R]): R = {
    import cats.syntax.show._
    decode[R](entity) match {
      case Left(error) =>
        logger.debug(s"response can't be decoded to entity : '${error.show}'")
        decode[AuthFlow](entity) match {
          case Left(error) =>
            logger.debug(s"response can't be decoded to AuthFlow: '${error.show}'")
            decode[ApiError](entity) match {
              case Left(error) =>
                logger.debug(s"Unexpected response: $entity")
                logger.warn(s"Unexpected response (JSON decoding failed): '${error.show}'")
                throw new ApiFailure(
                  "Failed to decode server response",
                  "ORG.MASCARENE.DECODE_ERROR",
                  Some(error.toString + entity)
                )
              case Right(servererror) =>
                logger.debug(s"Server error response: $servererror")
                throw new ApiFailure(s"$servererror", servererror.errcode, servererror.error)
            }

          case Right(flow) =>
            logger.debug(s"Auth flow response: $flow")
            throw new AuthFlowException(
              s"$flow",
              flow.completed,
              flow.errcode,
              flow.error,
              flow.flows,
              flow.params,
              flow.session
            )
        }
      case Right(t) => t
    }
  }

}
