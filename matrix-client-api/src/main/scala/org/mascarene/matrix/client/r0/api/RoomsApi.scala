/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.matrix.client.r0.api

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.model.Uri
import io.circe.generic.auto._
import org.mascarene.matrix.client.r0.Transport
import org.mascarene.matrix.client.r0.model.auth.AuthToken
import org.mascarene.matrix.client.r0.model.rooms._

import scala.concurrent.Future

trait RoomsApi { self: Transport =>
  implicit def actorSystem: ActorSystem[Nothing]
  implicit private val ec = actorSystem.executionContext

  protected def apiRoot: Uri

  def createRoom(request: CreateRoomRequest)(implicit token: AuthToken): Future[CreateRoomResponse] =
    doPut[CreateRoomRequest, CreateRoomResponse](apiRoot.withPath(apiRoot.path + s"/r0/createRoom"), request)
      .map(_.entity)

  def createRoomAlias(roomId: String, roomAlias: String)(implicit token: AuthToken): Future[Unit] =
    doPut[CreateRoomAliasRequest, Unit](
      apiRoot.withPath(apiRoot.path + s"/r0/directory/room/$roomAlias"),
      CreateRoomAliasRequest(roomId)
    ).map(_.entity)

  def resolveRoomAlias(roomAlias: String)(implicit token: Option[AuthToken] = None): Future[ResolveRoomResponse] =
    doGet[ResolveRoomResponse](apiRoot.withPath(apiRoot.path + s"/r0/directory/room/$roomAlias")).map(_.entity)

  def deleteRoomAlias(roomAlias: String)(implicit token: AuthToken): Future[Unit] =
    doDelete[Unit](apiRoot.withPath(apiRoot.path + s"/r0/directory/room/$roomAlias")).map(_.entity)

  def joinedRooms(implicit token: AuthToken): Future[JoinedRoomsResponse] =
    doGet[JoinedRoomsResponse](apiRoot.withPath(apiRoot.path + "/r0/joined_rooms")).map(_.entity)

  def invite(roomId: String, userId: String)(implicit token: AuthToken): Future[Unit] =
    doPost[InviteRoomRequest, Unit](
      apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/invite"),
      InviteRoomRequest(userId)
    ).map(_.entity)

  def joinByRoomId(roomId: String, signature: Option[ThirdPartySigned])(implicit
      token: AuthToken
  ): Future[JoinRoomResponse] =
    doPost[JoinRoomRequest, JoinRoomResponse](
      apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/join"),
      JoinRoomRequest(signature)
    ).map(_.entity)
  def joinByRoomIdOrAlias(roomIdOrAlias: String, signature: Option[ThirdPartySigned])(implicit
      token: AuthToken
  ): Future[JoinRoomResponse] =
    doPost[JoinRoomRequest, JoinRoomResponse](
      apiRoot.withPath(apiRoot.path + s"/r0/join/$roomIdOrAlias"),
      JoinRoomRequest(signature)
    ).map(_.entity)

  def leave(roomId: String)(implicit token: AuthToken): Future[Unit] =
    doPost[Unit, Unit](apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/leave"), ()).map(_.entity)

  def forget(roomId: String)(implicit token: AuthToken): Future[Unit] =
    doPost[Unit, Unit](apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/forget"), ()).map(_.entity)

  def kick(roomId: String, userId: String, reason: Option[String] = None)(implicit token: AuthToken): Future[Unit] =
    doPost[KickUserRequest, Unit](
      apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/kick"),
      KickUserRequest(userId, reason)
    ).map(_.entity)

  def ban(roomId: String, userId: String, reason: Option[String] = None)(implicit token: AuthToken): Future[Unit] =
    doPost[BanUserRequest, Unit](
      apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/ban"),
      BanUserRequest(userId, reason)
    ).map(_.entity)

  def unban(roomId: String, userId: String)(implicit token: AuthToken): Future[Unit] =
    doPost[UnBanUserRequest, Unit](
      apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/unban"),
      UnBanUserRequest(userId)
    ).map(_.entity)

  def listPublicRooms(
      limit: Option[Int] = None,
      since: Option[String] = None,
      server: Option[String] = None
  ): Future[ListPublicRoomsResponse] = {
    val query = List(
      limit.map(v => "limit" -> v.toString),
      since.map(v => "since" -> v),
      server.map(v => "server" -> v)
    ).filter(_.isDefined).map(_.get).toMap
    doGet[ListPublicRoomsResponse](apiRoot.withPath(apiRoot.path + s"/r0/publicRooms"), Seq.empty, query).map(_.entity)
  }

  def listPublicRoomsWithFilter(
      limit: Option[Int] = None,
      since: Option[String] = None,
      search_term: Option[String] = None,
      server: Option[String] = None,
      includeAllNetworks: Option[Boolean] = Some(false),
      thirdPartyInstanceId: Option[String] = None
  )(implicit token: AuthToken): Future[ListPublicRoomsResponse] = {
    val query = List(
      server.map(v => "server" -> v)
    ).filter(_.isDefined).map(_.get).toMap
    doPost[ListPublicRoomsWithFilterRequest, ListPublicRoomsResponse](
      apiRoot.withPath(apiRoot.path + s"/r0/publicRooms"),
      ListPublicRoomsWithFilterRequest(
        limit,
        since,
        search_term.map(t => Filter(Some(t))),
        includeAllNetworks,
        thirdPartyInstanceId
      ),
      Seq.empty,
      query
    ).map(_.entity)
  }
}
