/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.matrix.client.r0

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.model.Uri
import org.mascarene.matrix.client.r0.api.{AccountApi, AuthApi, BaseApi, FilterApi}

import scala.concurrent.ExecutionContextExecutor

class MatrixClient(baseUrl: String)(implicit val actorSystem: ActorSystem[Nothing])
    extends HttpTransport
    with BaseApi
    with AuthApi
    with AccountApi
    with FilterApi
    with ClientResponseDecodeHandler {

  implicit val executionContext: ExecutionContextExecutor = actorSystem.executionContext
  protected def apiRoot                                   = Uri(baseUrl).withPath(Uri.Path("/_matrix/client"))

}
