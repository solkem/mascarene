/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
import sbt._

object Dependencies {
  object V {
    val circe         = "0.13.0"
    val circeOptics   = "0.13.0"
    val akkaHttp      = "10.2.3"
    val akka          = "2.6.11"
    val quillJdbc     = "3.6.0"
    val flyway        = "7.5.1"
    val postgresql    = "42.2.18"
    val scalaTest     = "3.2.3"
    val commonsCodec  = "1.15"
    val commonsLang   = "3.11"
    val argon2        = "2.9"
    val scalaGraph    = "1.13.2"
    val scalaGraphDot = "1.13.0"
    val slf4j         = "1.7.25"
    val pureConfig    = "0.14.0"
    val logback       = "1.2.3"
    val scalaLogging  = "3.9.2"
    val akkaHttpCirce = "1.35.3"
    val uaparser      = "0.11.0"
    val jwt           = "5.0.0"
    val akkaCors      = "1.1.1"
    val scopt         = "4.0.0"
    val kryo          = "1.1.5"
    val scaffeine     = "4.0.2"
    val guava         = "30.1-jre"
    val bouncyCastle  = "1.68"
  }

  val commonDependencies: Seq[ModuleID] = Seq(
    "com.typesafe.scala-logging" %% "scala-logging"            % V.scalaLogging,
    "ch.qos.logback"              % "logback-classic"          % V.logback,
    "com.typesafe.akka"          %% "akka-http"                % V.akkaHttp,
    "com.typesafe.akka"          %% "akka-stream"              % V.akka,
    "com.typesafe.akka"          %% "akka-slf4j"               % V.akka,
    "de.heikoseeberger"          %% "akka-http-circe"          % V.akkaHttpCirce,
    "io.circe"                   %% "circe-optics"             % V.circeOptics,
    "com.typesafe.akka"          %% "akka-http-testkit"        % V.akkaHttp  % Test,
    "com.typesafe.akka"          %% "akka-stream-testkit"      % V.akka      % Test,
    "com.typesafe.akka"          %% "akka-actor-testkit-typed" % V.akka      % Test,
    "org.scalatest"              %% "scalatest"                % V.scalaTest % Test
  ) ++ Seq(
    "io.circe" %% "circe-core",
    "io.circe" %% "circe-generic",
    "io.circe" %% "circe-parser"
  ).map(_ % V.circe)

  val sdkDependencies: Seq[ModuleID] = commonDependencies ++ Seq()

  val hsDependencies: Seq[ModuleID] = commonDependencies ++ Seq(
    "io.altoo"              %% "akka-kryo-serialization"     % V.kryo,
    "com.typesafe.akka"     %% "akka-actor-typed"            % V.akka,
    "com.typesafe.akka"     %% "akka-actor"                  % V.akka,
    "com.typesafe.akka"     %% "akka-stream-typed"           % V.akka,
    "com.typesafe.akka"     %% "akka-cluster-typed"          % V.akka,
    "com.typesafe.akka"     %% "akka-cluster-sharding-typed" % V.akka,
    "org.flywaydb"           % "flyway-core"                 % V.flyway,
    "io.getquill"           %% "quill-jdbc"                  % V.quillJdbc,
    "org.postgresql"         % "postgresql"                  % V.postgresql,
    "de.mkammerer"           % "argon2-jvm"                  % V.argon2,
    "org.scala-graph"       %% "graph-core"                  % V.scalaGraph,
    "org.scala-graph"       %% "graph-dot"                   % V.scalaGraphDot,
    "com.github.pureconfig" %% "pureconfig"                  % V.pureConfig,
    "com.pauldijou"         %% "jwt-circe"                   % V.jwt,
    "org.uaparser"          %% "uap-scala"                   % V.uaparser,
    "ch.megard"             %% "akka-http-cors"              % V.akkaCors,
    "org.apache.commons"     % "commons-lang3"               % V.commonsLang,
    "com.github.scopt"      %% "scopt"                       % V.scopt,
    "com.google.guava"       % "guava"                       % V.guava,
    "com.github.blemale"    %% "scaffeine"                   % V.scaffeine,
    "org.bouncycastle"       % "bcprov-jdk15on"              % V.bouncyCastle
  )

  val matrixApiDependencies: Seq[ModuleID] = commonDependencies ++ Seq(
    "com.google.guava"   % "guava"            % V.guava,
    "com.typesafe.akka" %% "akka-actor-typed" % V.akka,
    "org.bouncycastle"   % "bcprov-jdk15on"   % V.bouncyCastle
  )

  val hsSpiDependencies: Seq[ModuleID] = commonDependencies ++ Seq(
    "com.typesafe.akka"     %% "akka-actor-typed"            % V.akka,
    "com.typesafe.akka"     %% "akka-stream-typed"           % V.akka,
    "com.typesafe.akka"     %% "akka-cluster-typed"          % V.akka,
    "com.typesafe.akka"     %% "akka-cluster-sharding-typed" % V.akka,
    "io.getquill"           %% "quill-jdbc"                  % V.quillJdbc,
    "de.mkammerer"           % "argon2-jvm"                  % V.argon2,
    "com.github.pureconfig" %% "pureconfig"                  % V.pureConfig,
    "com.pauldijou"         %% "jwt-circe"                   % V.jwt,
    "org.uaparser"          %% "uap-scala"                   % V.uaparser,
    "ch.megard"             %% "akka-http-cors"              % V.akkaCors,
    "com.google.guava"       % "guava"                       % V.guava,
    "com.github.blemale"    %% "scaffeine"                   % V.scaffeine,
    "org.bouncycastle"       % "bcprov-jdk15on"              % V.bouncyCastle
  )

  val utilsDependencies: Seq[ModuleID] = commonDependencies ++ Seq(
    "com.typesafe.akka" %% "akka-actor"    % V.akka,
    "commons-codec"      % "commons-codec" % V.commonsCodec,
    "com.typesafe.akka" %% "akka-testkit"  % V.akka % Test
  )
}
