package org.mascarene.matrix.server.r0.model

import io.circe.Json

case class RequestSignature(
    method: String,
    uri: String,
    origin: String,
    destination: String,
    content: Option[Json] = None,
    signatures: Map[String, Map[String, String]] = Map.empty
)
