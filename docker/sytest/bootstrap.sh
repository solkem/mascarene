#!/bin/bash
#
set -ex

echo "--- Trying to get same-named sytest branch..."

# Check if we're running in buildkite, if so it can tell us what
# Synapse/Dendrite branch we're running
if [ -n "$BUILDKITE_BRANCH" ]; then
    branch_name=$BUILDKITE_BRANCH
else
    # Otherwise, try and find the branch that the Synapse/Dendrite checkout
    # is using. Fall back to develop if unknown.
    branch_name="$(git --git-dir=/src/.git symbolic-ref HEAD 2>/dev/null)" || branch_name="develop"
fi

# Try and fetch the branch
wget -q https://github.com/matrix-org/sytest/archive/$branch_name.tar.gz -O sytest.tar.gz || {
    # Probably a 404, fall back to develop
    echo "Using develop instead..."
    wget -q https://github.com/matrix-org/sytest/archive/develop.tar.gz -O sytest.tar.gz
}

mkdir -p /sytest
tar -C /sytest --strip-components=1 -xf sytest.tar.gz

echo "--- Preparing sytest"

export SYTEST_LIB="/sytest/lib"

