# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/) and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 0.3.0-SNAPSHOT
### Added

Mascarene server replies with the `Server` HTTP header set to `mascarene/0.3.0-SNAPSHOT`.
Mascarene client sends requests with the `User-Agent` HTTP header set to `mascarene/0.3.0-SNAPSHOT`.

Federation server part:
- implement [server names resolution rules](https://matrix.org/docs/spec/server_server/r0.1.4#resolving-server-names)
- implement `GET /.well-known/matrix/server` endpoint ([r0.1.4](https://matrix.org/docs/spec/server_server/r0.1.4#get-well-known-matrix-server))
- implement `GET /_matrix/federation/v1/version` endpoint ([r0.1.4](https://matrix.org/docs/spec/server_server/r0.1.4#get-matrix-federation-v1-version))

Federation client part:
- implement `GET /.well-known/matrix/server` endpoint ([r0.1.4](https://matrix.org/docs/spec/server_server/r0.1.4#get-well-known-matrix-server))
- implement `GET /_matrix/federation/v1/version` endpoint ([r0.1.4](https://matrix.org/docs/spec/server_server/r0.1.4#get-matrix-federation-v1-version))
- implement `GET /_matrix/key/v2/server/{keyId}` endpoint ([r0.1.4](https://matrix.org/docs/spec/server_server/r0.1.4#get-matrix-key-v2-server-keyid))
- implement `GET /_matrix/key/v2/query/{serverName}/{keyId}` endpoint ([r0.1.4](https://matrix.org/docs/spec/server_server/r0.1.4#get-matrix-key-v2-query-servername-keyid))


### Changed
- Dependencies updates:
  - `sbt-sonatype` plugin updated from `3.9.2` to `3.9.3`
  - `sbt-native-packager` plugin updated from `1.7.2` to `1.7.3`
  - `sbt-updates` plugin updated from `0.5.0` to `0.5.1`

### Removed
None

## [0.2.0](https://gitlab.com/mascarene/mascarene/-/releases/v0.2.0) - 2020-06-21

### Added
- implementation for `GET /_matrix/client/r0/rooms/{roomId}/joined_members` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-r0-rooms-roomid-joined-members))
- implementation for `GET /_matrix/client/r0/rooms/{roomId}/state` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-r0-rooms-roomid-state))
- implementation for `GET /_matrix/client/r0/rooms/{roomId}/state/{eventType}/{stateKey}` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-r0-rooms-roomid-state-eventtype-statekey))
- implementation for `GET /_matrix/client/r0/rooms/{roomId}/event/{eventId}` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-r0-rooms-roomid-event-eventid))
- implementation for `POST /_matrix/client/r0/account/deactivate` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#post-matrix-client-r0-account-deactivate))
- implementation for room membership API endpoints ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#room-membership))
  - `GET /_matrix/client/r0/joined_rooms`
  - `POST /_matrix/client/r0/rooms/{roomId}/invite`
  - `POST /_matrix/client/r0/rooms/{roomId}/join`
  - `POST /_matrix/client/r0/rooms/{roomId}/leave`
  - `POST /_matrix/client/r0/rooms/{roomId}/forget`
  - `POST /_matrix/client/r0/rooms/{roomId}/kick`
  - `POST /_matrix/client/r0/rooms/{roomId}/ban`
  - `POST /_matrix/client/r0/rooms/{roomId}/unban`
  - `POST /_matrix/client/r0/join/{roomIdOrAlias}`
  - `GET /_matrix/client/r0/publicRooms` (mock only)
  - `POST /_matrix/client/r0/publicRooms` (mock only)
  - `GET /_matrix/client/r0/directory/list/room/{roomId}`
  - `PUT /_matrix/client/r0/directory/list/room/{roomId}`
- implementation for `GET /_matrix/client/r0/directory/list/room/{roomId}` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-r0-directory-list-room-roomid))
- implementation for `PUT /_matrix/client/r0/directory/list/room/{roomId}` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#put-matrix-client-r0-directory-list-room-roomid))

### Changed
- Use [java.time.Instant](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/time/Instant.html) for point in time data like event timestamps. Persistence now uses `TIMESTAMP WITH TIME ZONE`.
- `GET /sync` returns `transaction_id` for each event linked with a client transaction if the client being given the event is the same one which sent it.
- Dependencies updates:
  - `scalafmt` updated from `2.4.2` to `2.6.1`

### Removed
None

## [0.1.0](https://gitlab.com/mascarene/mascarene/-/releases/v0.1.0) - 2020-06-14
### Added
- implementation for user profiles API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#profiles))
  - `PUT /_matrix/client/r0/profile/{userId}/displayname` 
  - `GET /_matrix/client/r0/profile/{userId}/displayname` 
  - `PUT /_matrix/client/r0/profile/{userId}/avatar_url` 
  - `GET /_matrix/client/r0/profile/{userId}/avatar_url` 
  - `GET /_matrix/client/r0/profile/{userId}` 
- implementation for `POST /_matrix/client/r0/user_directory/search` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#post-matrix-client-r0-user-directory-search))
- [#20](https://gitlab.com/mascarene/mascarene/-/issues/20) implementation for `GET /_matrix/client/r0/register/available` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-r0-register-available))
- rate limitation for API endpoints
- implementation for `GET /_matrix/client/r0/directory/room/{roomAlias}` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-r0-directory-room-roomalias))
- implementation for `PUT /_matrix/client/r0/rooms/{roomId}/state/{eventType}/{stateKey}` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#put-matrix-client-r0-rooms-roomid-state-eventtype-statekey))
- [#10](https://gitlab.com/mascarene/mascarene/-/issues/10) Refactor cache implementation to use [Scaffeine](https://github.com/blemale/scaffeine) and distributed data instead of external Redis. 
- implementation for `GET /_matrix/client/r0/capabilities` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#id227)) 
- implementation for `GET /_matrix/client/versions` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-versions)) 
- implementation for `GET /.well-known/matrix/client` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-well-known-matrix-client)) 
- implementation for `GET /_matrix/client/r0/login` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-r0-login))
- implementation for `POST /_matrix/client/r0/login` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#post-matrix-client-r0-login))
- implementation for `POST /_matrix/client/r0/logout` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#post-matrix-client-r0-logout))
- implementation for `POST /_matrix/client/r0/logout/all` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#post-matrix-client-r0-logout-all))
- implementation for `POST /_matrix/client/r0/register` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#post-matrix-client-r0-register))
- implementation for `GET /_matrix/client/r0/account/whoami` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-r0-account-whoami))
- implementation for `POST /_matrix/client/r0/user/{userId}/filter` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#post-matrix-client-r0-user-userid-filter))
- implementation for `GET /_matrix/client/r0/user/{userId}/filter/{filterId}` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-r0-user-userid-filter-filterid))
- implementation for `GET /_matrix/client/r0/sync` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-r0-sync)) **Partially implemented**: ephemeral, presence and filtering are not implemented
- implementation for `POST /_matrix/client/r0/createRoom` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#post-matrix-client-r0-createroom))
- implementation for `PUT /_matrix/client/r0/rooms/{roomId}/send/{eventType}/{txnId}` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#put-matrix-client-r0-user-userid-account-data-type))
- implementation for `PUT /_matrix/client/r0/user/{userId}/account_data/{type}` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#put-matrix-client-r0-user-userid-account-data-type))
- implementation for `GET /_matrix/client/r0/user/{userId}/account_data/{type}` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-r0-user-userid-account-data-type))
- implementation for `PUT /_matrix/client/r0/user/{userId}/rooms/{roomId}/account_data/{type}` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#put-matrix-client-r0-user-userid-rooms-roomid-account-data-type))
- implementation for `GET /_matrix/client/r0/user/{userId}/rooms/{roomId}/account_data/{type}` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-r0-user-userid-rooms-roomid-account-data-type))

### Changed
None

### Removed
None