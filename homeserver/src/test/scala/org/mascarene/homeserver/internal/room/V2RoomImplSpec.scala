/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.room

import java.time.Instant
import java.util.UUID
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import org.mascarene.homeserver.internal.rooms.{PowerLevelsUtils, V2RoomImpl}
import org.mascarene.matrix.client.r0.model.rooms.{CreationContent, PowerLevelEventContent}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import io.circe.generic.auto._
import io.circe.syntax._
import org.mascarene.homeserver.TestEnv
import org.mascarene.homeserver.internal.model.{EventTypes, Room, User}
import org.mascarene.homeserver.internal.repository.{AuthRepo, EventRepo, RoomRepo}
import org.mascarene.matrix.client.r0.model.events.MemberEventContent
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll}

import scala.util.{Failure, Success}

class V2RoomImplSpec
    extends AnyWordSpecLike
    with Matchers
    with FailFastCirceSupport
    with TestEnv
    with BeforeAndAfter
    with BeforeAndAfterAll {

  private[this] val eventRepo = new EventRepo
  private[this] val authRepo  = new AuthRepo
  private[this] val roomRepo  = new RoomRepo

  flyway.clean()
  flyway.migrate()
  override def afterAll(): Unit = testKit.shutdownTestKit()

  var sequence = 0L

  private def insertCreateEvent(mxEventId: String, room: Room, sender: User) = {
    sequence += 1
    eventRepo
      .insertEvent(
        room.roomId,
        sender.userId,
        EventTypes.M_ROOM_CREATE,
        Some(""),
        Some(CreationContent(creator = sender.mxUserId, room_version = Some("5")).asJson),
        mxEventId = s"$$$mxEventId",
        originServerTs = Instant.now(),
        streamOrder = sequence
      )
      .get
  }

  private def insertMemberEvent(
      mxEventId: String,
      room: Room,
      sender: User,
      membership: String,
      granterMember: User,
      parentsId: Set[UUID],
      authEventsId: Set[UUID]
  ) = {
    sequence += 1
    eventRepo
      .insertEvent(
        room.roomId,
        sender.userId,
        EventTypes.M_ROOM_MEMBER,
        Some(granterMember.mxUserId),
        Some(MemberEventContent(membership = membership).asJson),
        mxEventId = s"$$$mxEventId",
        originServerTs = Instant.now(),
        parentsId = parentsId,
        authsEventId = authEventsId,
        streamOrder = sequence
      )
      .get
  }

  private def insertPLEvent(
      mxEventId: String,
      room: Room,
      sender: User,
      parentsId: Set[UUID],
      authEventsId: Set[UUID],
      plContent: PowerLevelEventContent
  ) = {
    sequence += 1
    eventRepo
      .insertEvent(
        room.roomId,
        sender.userId,
        EventTypes.M_ROOM_POWER_LEVELS,
        Some(""),
        Some(plContent.asJson),
        mxEventId = s"$$$mxEventId",
        originServerTs = Instant.now(),
        parentsId = parentsId,
        authsEventId = authEventsId,
        streamOrder = sequence
      )
      .get
  }

  private def insertJoinRulesEvent(
      mxEventId: String,
      room: Room,
      sender: User,
      joinRule: String,
      parentsId: Set[UUID],
      authEventsId: Set[UUID]
  ) = {
    sequence += 1
    eventRepo
      .insertEvent(
        room.roomId,
        sender.userId,
        EventTypes.M_ROOM_JOIN_RULES,
        Some(""),
        Some(Map("join_rule" -> joinRule).asJson),
        mxEventId = s"$$$mxEventId",
        originServerTs = Instant.now(),
        parentsId = parentsId,
        authsEventId = authEventsId,
        streamOrder = sequence
      )
      .get
  }

  "v2RoomImpl resolve algorithm" should {

    val alice  = authRepo.createUser("@alice:example.com").get
    val bob    = authRepo.createUser("@bob:example.com").get
    val carol  = authRepo.createUser("@carol:example.com").get
    val evelyn = authRepo.createUser("@evelyn:example.com").get

    "resolve direct graph" in {
      val testRoom = roomRepo.createRoom("!room1:example.com", "public", "5").get

      val createEvent = insertCreateEvent("createRoom", testRoom, alice)
      val aliceJoinEvent =
        insertMemberEvent(
          "aliceJoin",
          testRoom,
          alice,
          "join",
          alice,
          Set(createEvent.eventId),
          Set(createEvent.eventId)
        )
      val powerLevelEvent = insertPLEvent(
        "powerLevels",
        testRoom,
        alice,
        Set(aliceJoinEvent.eventId),
        Set(aliceJoinEvent.eventId),
        PowerLevelsUtils.defaultPowerLevelContent.copy(users = Map(alice.mxUserId -> 100))
      )
      val joinRulesEvent =
        insertJoinRulesEvent(
          "joinRules",
          testRoom,
          alice,
          "private",
          Set(powerLevelEvent.eventId),
          Set(powerLevelEvent.eventId, aliceJoinEvent.eventId)
        )
      val inviteBob =
        insertMemberEvent(
          "aliceInvitesBob",
          testRoom,
          alice,
          "invite",
          bob,
          Set(joinRulesEvent.eventId),
          Set(aliceJoinEvent.eventId, powerLevelEvent.eventId)
        )
      val inviteCarol =
        insertMemberEvent(
          "aliceInvitesCarol",
          testRoom,
          alice,
          "invite",
          carol,
          Set(inviteBob.eventId),
          Set(aliceJoinEvent.eventId, powerLevelEvent.eventId)
        )
      val bobJoinEvent =
        insertMemberEvent(
          "bobJoin",
          testRoom,
          bob,
          "join",
          bob,
          Set(inviteCarol.eventId),
          Set(joinRulesEvent.eventId, inviteBob.eventId)
        )

      val algoImpl = new V2RoomImpl(testRoom)
      algoImpl.resolve(createEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          assert(stateSet.size == 1)
          stateSet.getEvent(EventTypes.M_ROOM_CREATE, "") shouldBe Some(createEvent)
          runtimeContext.eventStateSetCache.put(resolvedEvent.eventId, stateSet)
        case (_, Failure(f)) => fail(f)
      }

      algoImpl
        .resolve(aliceJoinEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          assert(stateSet.size == 2)
          stateSet.getEvent(EventTypes.M_ROOM_CREATE, "") shouldBe Some(createEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, alice.mxUserId) shouldBe Some(aliceJoinEvent)
          runtimeContext.eventStateSetCache.put(resolvedEvent.eventId, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl
        .resolve(powerLevelEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          assert(stateSet.size == 3)
          stateSet.getEvent(EventTypes.M_ROOM_CREATE, "") shouldBe Some(createEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, alice.mxUserId) shouldBe Some(aliceJoinEvent)
          stateSet.getEvent(EventTypes.M_ROOM_POWER_LEVELS, "") shouldBe Some(powerLevelEvent)
          runtimeContext.eventStateSetCache.put(resolvedEvent.eventId, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl
        .resolve(joinRulesEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          assert(stateSet.size == 4)
          stateSet.getEvent(EventTypes.M_ROOM_CREATE, "") shouldBe Some(createEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, alice.mxUserId) shouldBe Some(aliceJoinEvent)
          stateSet.getEvent(EventTypes.M_ROOM_POWER_LEVELS, "") shouldBe Some(powerLevelEvent)
          stateSet.getEvent(EventTypes.M_ROOM_JOIN_RULES, "") shouldBe Some(joinRulesEvent)
          runtimeContext.eventStateSetCache.put(resolvedEvent.eventId, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl
        .resolve(inviteBob) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          assert(stateSet.size == 5)
          stateSet.getEvent(EventTypes.M_ROOM_CREATE, "") shouldBe Some(createEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, alice.mxUserId) shouldBe Some(aliceJoinEvent)
          stateSet.getEvent(EventTypes.M_ROOM_POWER_LEVELS, "") shouldBe Some(powerLevelEvent)
          stateSet.getEvent(EventTypes.M_ROOM_JOIN_RULES, "") shouldBe Some(joinRulesEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, bob.mxUserId) shouldBe Some(inviteBob)
          runtimeContext.eventStateSetCache.put(resolvedEvent.eventId, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl
        .resolve(inviteCarol) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          assert(stateSet.size == 6)
          stateSet.getEvent(EventTypes.M_ROOM_CREATE, "") shouldBe Some(createEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, alice.mxUserId) shouldBe Some(aliceJoinEvent)
          stateSet.getEvent(EventTypes.M_ROOM_POWER_LEVELS, "") shouldBe Some(powerLevelEvent)
          stateSet.getEvent(EventTypes.M_ROOM_JOIN_RULES, "") shouldBe Some(joinRulesEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, carol.mxUserId) shouldBe Some(inviteCarol)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, bob.mxUserId) shouldBe Some(inviteBob)
          runtimeContext.eventStateSetCache.put(resolvedEvent.eventId, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl
        .resolve(bobJoinEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          assert(stateSet.size == 6)
          stateSet.getEvent(EventTypes.M_ROOM_CREATE, "") shouldBe Some(createEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, alice.mxUserId) shouldBe Some(aliceJoinEvent)
          stateSet.getEvent(EventTypes.M_ROOM_POWER_LEVELS, "") shouldBe Some(powerLevelEvent)
          stateSet.getEvent(EventTypes.M_ROOM_JOIN_RULES, "") shouldBe Some(joinRulesEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, carol.mxUserId) shouldBe Some(inviteCarol)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, bob.mxUserId) shouldBe Some(bobJoinEvent)
          runtimeContext.eventStateSetCache.put(resolvedEvent.eventId, stateSet)
        case (_, Failure(f)) => fail(f)
      }
    }

    "resolve ban vs power level" in {
      val testRoom = roomRepo.createRoom("!room2:example.com", "public", "5").get

      val createEvent = insertCreateEvent("CREATE", testRoom, alice)
      val aliceJoinEvent =
        insertMemberEvent("IMA", testRoom, alice, "join", alice, Set(createEvent.eventId), Set(createEvent.eventId))
      val powerLevelEvent = insertPLEvent(
        "IPOWER",
        testRoom,
        alice,
        Set(aliceJoinEvent.eventId),
        Set(createEvent.eventId, aliceJoinEvent.eventId),
        PowerLevelsUtils.defaultPowerLevelContent.copy(users = Map(alice.mxUserId -> 100))
      )
      val joinRulesEvent =
        insertJoinRulesEvent(
          "IJR",
          testRoom,
          alice,
          "public",
          Set(powerLevelEvent.eventId),
          Set(createEvent.eventId, powerLevelEvent.eventId, aliceJoinEvent.eventId)
        )
      val bobJoinEvent =
        insertMemberEvent(
          "IMB",
          testRoom,
          bob,
          "join",
          bob,
          Set(joinRulesEvent.eventId),
          Set(createEvent.eventId, joinRulesEvent.eventId, powerLevelEvent.eventId)
        )
      val carolJoinEvent =
        insertMemberEvent(
          "IMC",
          testRoom,
          carol,
          "join",
          carol,
          Set(bobJoinEvent.eventId),
          Set(createEvent.eventId, joinRulesEvent.eventId, powerLevelEvent.eventId)
        )
      val plA = insertPLEvent(
        "PA",
        testRoom,
        alice,
        Set.empty,
        Set(createEvent.eventId, aliceJoinEvent.eventId, powerLevelEvent.eventId),
        PowerLevelsUtils.defaultPowerLevelContent.copy(users = Map(alice.mxUserId -> 100, bob.mxUserId -> 50))
      )
      val plB = insertPLEvent(
        "PB",
        testRoom,
        alice,
        Set(carolJoinEvent.eventId),
        Set(createEvent.eventId, aliceJoinEvent.eventId, powerLevelEvent.eventId),
        PowerLevelsUtils.defaultPowerLevelContent.copy(users = Map(alice.mxUserId -> 100, bob.mxUserId -> 50))
      )

      val aliceBanEvelynEvent =
        insertMemberEvent(
          "MB",
          testRoom,
          alice,
          "ban",
          evelyn,
          Set(plA.eventId),
          Set(createEvent.eventId, aliceJoinEvent.eventId, plB.eventId)
        )
      val evelynJoin =
        insertMemberEvent(
          "IME",
          testRoom,
          evelyn,
          "join",
          evelyn,
          Set(aliceBanEvelynEvent.eventId),
          Set(createEvent.eventId, joinRulesEvent.eventId, plA.eventId)
        )

      val algoImpl = new V2RoomImpl(testRoom)
      algoImpl.resolve(createEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          runtimeContext.eventStateSetCache.put(resolvedEvent.eventId, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl.resolve(aliceJoinEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          runtimeContext.eventStateSetCache.put(resolvedEvent.eventId, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl.resolve(powerLevelEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          runtimeContext.eventStateSetCache.put(resolvedEvent.eventId, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl.resolve(joinRulesEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          runtimeContext.eventStateSetCache.put(resolvedEvent.eventId, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl.resolve(bobJoinEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          runtimeContext.eventStateSetCache.put(resolvedEvent.eventId, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl.resolve(carolJoinEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          runtimeContext.eventStateSetCache.put(resolvedEvent.eventId, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl.resolve(plA) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          runtimeContext.eventStateSetCache.put(resolvedEvent.eventId, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl.resolve(plB) match {
        case (resolvedEvent, Success(stateSet)) =>
          //assert(resolvedEvent.rejected)
          //TODO : add test assertion when test case will be clarified
          runtimeContext.eventStateSetCache.put(resolvedEvent.eventId, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl.resolve(aliceBanEvelynEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          //TODO : add test assertion when test case will be clarified
          runtimeContext.eventStateSetCache.put(resolvedEvent.eventId, stateSet)
        case (_, Failure(f)) => fail(f)
      }

      algoImpl
        .resolve(evelynJoin) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(resolvedEvent.rejected)
          //TODO : add test assertion when test case will be clarified
          runtimeContext.eventStateSetCache.put(resolvedEvent.eventId, stateSet)
        case (_, Failure(f)) => fail(f)
      }
    }
  }
}
