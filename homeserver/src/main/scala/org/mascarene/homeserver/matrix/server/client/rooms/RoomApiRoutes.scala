/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.server.client.rooms

import akka.NotUsed
import akka.actor.typed.scaladsl.AskPattern._
import akka.actor.typed.{ActorRef, ActorSystem}
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.scaladsl.{Sink, Source}
import com.google.common.util.concurrent.RateLimiter
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import io.circe.syntax._
import io.circe.parser._
import io.circe.{Json, Printer}
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.internal.model.{Room, StateSet}
import org.mascarene.homeserver.internal.rooms.{
  GetRoomAgent,
  GetRoomState,
  GotRoomAgent,
  RoomClusterCommand,
  RoomStateResponse
}
import org.mascarene.homeserver.matrix.server
import org.mascarene.homeserver.matrix.server.{ApiErrorRejection, client}
import org.mascarene.homeserver.matrix.server.client.auth.AuthDirectives
import org.mascarene.matrix.client.r0.model.rooms.{
  CreateRoomRequest,
  CreateRoomResponse,
  GetRoomDirectoryResponse,
  ListPublicRoomsResponse,
  ListPublicRoomsWithFilterRequest,
  PublicRoomsChunk,
  PutRoomDirectoryRequest
}
import org.mascarene.matrix.client.r0.validation.rooms.RoomsValidator.ValidationResult
import org.mascarene.matrix.client.r0.validation.rooms.{InvalidVisibilityValue, RoomsValidator, UnsupportedRoomVersion}
import org.mascarene.sdk.matrix.core.{ApiError, ApiErrors}
import org.mascarene.utils.Codecs

import java.util.UUID
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.jdk.CollectionConverters._
import scala.jdk.DurationConverters._
import scala.util.{Failure, Success}

class RoomApiRoutes(
    system: ActorSystem[Nothing],
    val roomCluster: ActorRef[RoomClusterCommand],
    clientApiRateLimiter: RateLimiter
)(implicit
    val runtimeContext: RuntimeContext
) extends FailFastCirceSupport
    with AuthDirectives
    with client.ThrottleDirective
    with ImplicitAskTimeOut
    with LazyLogging {
  implicit private val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)
  implicit val sys                      = system
  protected val authService             = runtimeContext.newAuthService
  private val roomService               = runtimeContext.newRoomService
  private val statesetService           = runtimeContext.newStateSetService
  private val apiTransactionService     = runtimeContext.newApiTransactionService

  private val defaultRoomVersion = runtimeContext.config.getString("mascarene.matrix.default-room-version")
  private val supportedRoomVersions =
    runtimeContext.config.getStringList("mascarene.matrix.supported-room-versions").asScala.toList
  private val throttleWaitTime =
    runtimeContext.config.getDuration("mascarene.server.client-api.throttle-wait-time").toScala

  lazy val routes: Route = pathPrefix("_matrix" / "client" / "r0") {
    path("createRoom") {
      post {
        requireAuth { credentials =>
          entity(as[CreateRoomRequest]) { request =>
            validateCreateRoomRequest(request)
              .map { validatedRequest =>
                // Start a new room server
                logger.info(s"API CALL - createRoom: $validatedRequest")
                val createFuture = for {
                  room <- Future.fromTry(
                    roomService.createRoom(validatedRequest.visibility.get, validatedRequest.room_version)
                  )
                  serverRef <-
                    roomCluster
                      .ask[GotRoomAgent](GetRoomAgent(room, _))
                      .map(_.serverRef)
                  _         <- roomService.postCreateRoomEvents(serverRef, credentials.user, validatedRequest)
                  roomState <- serverRef.ask[RoomStateResponse](replyTo => GetRoomState(replyTo))
                } yield roomState
                onComplete(createFuture) { completed =>
                  completed
                    .map {
                      case RoomStateResponse(roomState) =>
                        complete(CreateRoomResponse(room_id = roomState.room.mxRoomId))
                    }
                    .recover { failure => reject(ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage)))) }
                    .get
                }
              }
              .leftMap { chain =>
                logger.debug(s"CreateRoomRequest validation failed with result: $chain")
                reject(server.ApiErrorRejection(chain.head.error))
              }
              .getOrElse(reject(server.ApiErrorRejection(ApiErrors.InternalError())))
          }
        }
      }
    } ~
      path("rooms" / Segment / "send" / Segment / Segment) { (roomId, eventType, txnId) =>
        put {
          requireAuth { credentials =>
            extractMatchedPath { matchedPath =>
              entity(as[Option[Json]]) { content =>
                logger.info(s"API CALL - $matchedPath: $content")
                apiTransactionService.getApiTransaction(txnId) match {
                  case Success(Some(apiTransaction)) =>
                    logger.warn(s"existing transaction found for matching path $matchedPath: returning old result")
                    complete(apiTransaction.responseContent)
                  case _ =>
                    val roomIdTry = roomService.getRoomByMxId(roomId).map(_.get)
                    logger.debug(s"get room result: $roomIdTry")
                    roomIdTry
                      .map { room =>
                        onComplete(roomService.postEvent(room, credentials.user, eventType, content)) { completed =>
                          completed
                            .map { newEvent =>
                              val response = Map("event_id" -> newEvent.mxEventId)
                              apiTransactionService.storeApiTransaction(
                                matchedPath.toString,
                                txnId,
                                credentials.authToken,
                                newEvent,
                                response.asJson
                              )
                              complete(response)
                            }
                            .recover { failure =>
                              reject(server.ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage))))
                            }
                            .get
                        }
                      }
                      .recover { failure =>
                        reject(
                          server.ApiErrorRejection(
                            ApiErrors.Unrecognized(Some(s"Invalid roomId or unknown room: $roomId"))
                          )
                        )
                      }
                      .get
                }
              }
            }
          }
        }
      } ~
      path("rooms" / Segment / "state" / Segment ~ Slash.? ~ PathEnd) { (roomId, eventType) =>
        put {
          requireAuth { credentials =>
            extractMatchedPath { matchedPath =>
              entity(as[Option[Json]]) { content =>
                logger.info(s"API CALL - $matchedPath: $content")
                val roomIdTry = roomService.getRoomByMxId(roomId).map(_.get)
                roomIdTry
                  .map { room =>
                    onComplete(roomService.postStateEvent(room, credentials.user, eventType, "", content)) {
                      completed =>
                        completed
                          .map { e =>
                            val response = Map("event_id" -> e.mxEventId)
                            complete(response)
                          }
                          .recover { failure =>
                            reject(server.ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage))))
                          }
                          .get
                    }
                  }
                  .recover { failure =>
                    reject(
                      server.ApiErrorRejection(
                        ApiErrors.Unrecognized(Some(s"Invalid roomId or unknown room: $roomId"))
                      )
                    )
                  }
                  .get
              }
            }
          }
        }
      } ~
      path("rooms" / Segment / "state" / Segment / Segment) { (roomId, eventType, stateKey) =>
        put {
          requireAuth { credentials =>
            extractMatchedPath { matchedPath =>
              entity(as[Option[Json]]) { content =>
                logger.info(s"API CALL - $matchedPath: $content")
                val roomIdTry = roomService.getRoomByMxId(roomId).map(_.get)
                roomIdTry
                  .map { room =>
                    onComplete(roomService.postStateEvent(room, credentials.user, eventType, stateKey, content)) {
                      completed =>
                        completed
                          .map { e =>
                            val response = Map("event_id" -> e.mxEventId)
                            complete(response)
                          }
                          .recover { failure =>
                            reject(server.ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage))))
                          }
                          .get
                    }
                  }
                  .recover { failure =>
                    reject(
                      server.ApiErrorRejection(
                        ApiErrors.Unrecognized(Some(s"Invalid roomId or unknown room: $roomId"))
                      )
                    )
                  }
                  .get
              }
            }
          }
        }
      } ~
      path("rooms" / Segment / "read_markers") { roomMxId =>
        post {
          withThrottle(clientApiRateLimiter, throttleWaitTime) {
            requireAuth { credentials =>
              //TODO: implement https://matrix.org/docs/spec/client_server/r0.6.0#post-matrix-client-r0-rooms-roomid-read-markers
              complete()
            }
          }
        }
      } ~
      path("directory" / "room" / Segment) { roomAlias =>
        get {
          roomService
            .getRoomAlias(roomAlias)
            .map {
              case Some(response) =>
                complete(
                  GetRoomDirectoryResponse(
                    response.room.mxRoomId,
                    response.servers.map(_.serverDomain)
                  )
                )
              case None =>
                complete(StatusCodes.NotFound, ApiError("M_NOT_FOUND", Some(s"Room alias $roomAlias not found.")))
            }
            .recover { failure =>
              reject(
                server.ApiErrorRejection(
                  ApiErrors.Unrecognized(Some(s"Invalid request"))
                )
              )
            }
            .get
        } ~
          put {
            requireAuth { credentials =>
              entity(as[PutRoomDirectoryRequest]) { request =>
                val roomIdTry = roomService.getRoomByMxId(request.room_id).map(_.get)
                roomIdTry
                  .map { room =>
                    val postEventFuture = for {
                      serverRef <-
                        roomCluster
                          .ask[GotRoomAgent](GetRoomAgent(room, _))
                          .map(_.serverRef)
                      event <- roomService.addRoomAliasPostEvent(room, credentials.user, roomAlias)
                    } yield event
                    onComplete(postEventFuture) { completed =>
                      completed
                        .map { e => complete(()) }
                        .recover { failure =>
                          reject(server.ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage))))
                        }
                        .get
                    }
                  }
                  .recover { failure =>
                    reject(
                      server.ApiErrorRejection(
                        ApiErrors.Unrecognized(Some(s"Invalid request: ${failure.getMessage}"))
                      )
                    )
                  }
                  .get
              }
            }
          }
      } ~
      path("rooms" / Segment / "typing" / Segment) { (roomMxId, userMxId) =>
        put {
          withThrottle(clientApiRateLimiter, throttleWaitTime) {
            requireAuth { credentials =>
              //TODO: implement https://matrix.org/docs/spec/client_server/r0.6.0#put-matrix-client-r0-rooms-roomid-typing-userid
              complete()
            }
          }
        }
      } ~
      path("publicRooms") {
        // GET /_matrix/client/r0/publicRooms
        get {
          parameters("limit".as[Int].?, "since".as[String].?, "server".as[String].?) {
            case (limit, since, server) =>
              // Search public rooms matching search term and filter results with since parameter
              val responseFuture: Future[ListPublicRoomsResponse] = for {
                roomSeq  <- Future.fromTry { roomService.searchPublicRooms(None) }
                response <- buildPublicRoomResponse(roomSeq, since, limit)
              } yield response
              onComplete(responseFuture) {
                case Success(response) => complete(response)
                case Failure(ex)       => reject(ApiErrorRejection(ApiErrors.InternalError(Some(ex.getMessage))))
              }
          }
        } ~
          // POST /_matrix/client/r0/publicRooms
          post {
            parameters("server".as[String].?) { server =>
              entity(as[ListPublicRoomsWithFilterRequest]) { request =>
                // Search public rooms matching search term and filter results with since parameter
                val responseFuture: Future[ListPublicRoomsResponse] = for {
                  roomSeq <- Future.fromTry {
                    roomService.searchPublicRooms(request.filter.flatMap(_.generic_search_term))
                  }
                  response <- buildPublicRoomResponse(roomSeq, request.since, request.limit)
                } yield response
                onComplete(responseFuture) {
                  case Success(response) => complete(response)
                  case Failure(ex)       => reject(ApiErrorRejection(ApiErrors.InternalError(Some(ex.getMessage))))
                }
              }
            }
          }
      }
  }

  /**
    * Validate create room request fields and initialize defaults according spec if not provided
    * @param request
    * @return
    */
  private def validateCreateRoomRequest(request: CreateRoomRequest): ValidationResult[CreateRoomRequest] = {
    import cats.implicits._
    def fillRoomVersion(request: CreateRoomRequest): ValidationResult[CreateRoomRequest] = {
      request.room_version match {
        case Some(v) if supportedRoomVersions.contains(v) => request.validNec
        case None                                         => request.copy(room_version = Some(defaultRoomVersion)).validNec
        case _                                            => UnsupportedRoomVersion(supportedRoomVersions.map(v => s"'$v'").mkString_(",")).invalidNec
      }
    }
    def fillPreset(request: CreateRoomRequest): ValidationResult[CreateRoomRequest] = {
      request.preset match {
        case Some(preset) => request.validNec
        case None =>
          request.visibility match {
            case Some("public")  => request.copy(preset = Some("public_chat")).validNec
            case Some("private") => request.copy(preset = Some("private_chat")).validNec
            case _               => InvalidVisibilityValue.invalidNec
          }
      }
    }

    RoomsValidator.validate(request).andThen(fillPreset).andThen(fillRoomVersion)
  }

  private def buildPublicRoomResponse(
      roomSeq: Seq[Room],
      since: Option[String],
      limitOpt: Option[Int]
  ): Future[ListPublicRoomsResponse] = {
    case class BatchToken(limitRoomId: UUID, size: Int, direction: Char) {
      def toToken: String = Codecs.base64Encode(this.asJson.noSpaces)
    }
    object BatchToken {
      def apply(token: String) = parse(Codecs.base64Decode(token)).fold(error => throw error, _.as[BatchToken]).toTry
    }

    val totalRoomCount = roomSeq.size
    val limit          = limitOpt.getOrElse(totalRoomCount)
    val (roomChunk: Seq[Room], nextBatch: Option[String], prevBatch: Option[String]) =
      since.map(BatchToken.apply) match {
        case Some(Success(token)) =>
          //If since is provided the decoded contains contains :
          // - the first roomId which was returned in this batch
          // - the size of the batch
          // - the direction of the batch
          // Depending on the direction, these information are used to cut the list at the right position, right size
          // to get the batch content. It also computes next and prev batch tokens
          if (token.direction == 'f') {
            //This is a next token, take batch from forward list
            (
              roomSeq.dropWhile(!_.roomId.equals(token.limitRoomId)).slice(1, limit + 1),
              roomSeq
                .dropWhile(!_.roomId.equals(token.limitRoomId))
                .drop(limit + 1)
                .headOption
                .map(room => BatchToken(room.roomId, limit, 'f').toToken),
              roomSeq.reverse
                .dropWhile(!_.roomId.equals(token.limitRoomId))
                .drop(1)
                .headOption
                .map(room => BatchToken(room.roomId, limit, 'b').toToken)
            )
          } else if (token.direction == 'b') {
            //This is a next token, take batch from reversed list
            (
              roomSeq.reverse.dropWhile(!_.roomId.equals(token.limitRoomId)).slice(1, limit + 1),
              roomSeq
                .dropWhile(!_.roomId.equals(token.limitRoomId))
                .drop(1)
                .headOption
                .map(room => BatchToken(room.roomId, limit, 'f').toToken),
              roomSeq.reverse
                .dropWhile(!_.roomId.equals(token.limitRoomId))
                .drop(limit + 1)
                .headOption
                .map(room => BatchToken(room.roomId, limit, 'b').toToken)
            )
          }
        case _ =>
          (
            roomSeq.take(limit),
            roomSeq.drop(limit).headOption.map(room => BatchToken(room.roomId, limit, 'f').toToken),
            None
          )
      }

    publicRoomsResponseFlow(Source(roomChunk))
      .runWith(Sink.seq[PublicRoomsChunk])
      .map(chunckSeq => ListPublicRoomsResponse(chunckSeq.toList, nextBatch, prevBatch, Some(totalRoomCount)))

  }

  /**
    * Flow which fill PublicRoomsChunck from room state informations
    * @param source
    * @return
    */
  private def publicRoomsResponseFlow(source: Source[Room, NotUsed]): Source[PublicRoomsChunk, NotUsed] = {
    case class BuildPublicRoomResponseFlowElem(room: Room, stateSet: StateSet, chunck: PublicRoomsChunk)
    source
      .map { room =>
        //fill room state
        val roomStateSet = roomService.getRoomLastState(room).get
        BuildPublicRoomResponseFlowElem(room, roomStateSet, PublicRoomsChunk(room.mxRoomId))
      }
      .map { elem =>
        // fill room known aliases
        val aliases = roomService.getRoomKnownAliases(elem.room).map(aliases => aliases.map(_.alias)).get match {
          case l if l.isEmpty => None
          case l              => Some(l)
        }
        elem.copy(chunck = elem.chunck.copy(aliases = aliases))
      }
      .map { elem =>
        // fill room canonical alias
        val canonical_alias = statesetService.getCanonicalAlias(elem.stateSet).get.map(_.alias)
        elem.copy(chunck = elem.chunck.copy(canonical_alias = canonical_alias))
      }
      .map { elem =>
        // fill room name
        val name = statesetService.getRoomName(elem.stateSet).get.map(_.name)
        elem.copy(chunck = elem.chunck.copy(name = name))
      }
      .map { elem =>
        // fill room topic
        val topic = statesetService.getRoomTopic(elem.stateSet).get.map(_.topic)
        elem.copy(chunck = elem.chunck.copy(topic = topic))
      }
      .map { elem =>
        // fill room history visibility
        val historyVisibility =
          statesetService
            .getRoomHistoryVisibility(elem.stateSet)
            .get
            .exists(_.history_visibility.equals("world_readable"))
        elem.copy(chunck = elem.chunck.copy(world_readable = historyVisibility))
      }
      .map { elem =>
        // fill room guest access
        val guestAccess =
          statesetService.getRoomGuestAccess(elem.stateSet).get.exists(_.guest_access.equals("can_join"))
        elem.copy(chunck = elem.chunck.copy(guest_can_join = guestAccess))
      }
      .map { elem =>
        // fill room avatar URL
        val avatar =
          statesetService.getRoomAvatar(elem.stateSet).get.map(_.url)
        elem.copy(chunck = elem.chunck.copy(avatar_url = avatar))
      }
      .map { elem =>
        // fill room joined member count
        val memberCount = roomService.memberList(elem.stateSet, _.membership.equals("join")).size
        elem.copy(chunck = elem.chunck.copy(num_joined_members = memberCount))
      }
      .map(_.chunck)
  }

}
