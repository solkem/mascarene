/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.server.client

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCodes}
import akka.http.scaladsl.server.Directives.{pathPrefix, _}
import akka.http.scaladsl.server.Route
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import io.circe.{Json, Printer}
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.matrix.server
import org.mascarene.homeserver.matrix.server.ApiErrorRejection
import org.mascarene.homeserver.matrix.server.client.auth.AuthDirectives
import org.mascarene.homeserver.services.AuthService
import org.mascarene.sdk.matrix.core.ApiErrors

class AccountDataApiRoutes(system: ActorSystem[Nothing])(implicit val runtimeContext: RuntimeContext)
    extends FailFastCirceSupport
    with AuthDirectives
    with ImplicitAskTimeOut
    with LazyLogging {
  implicit val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)

  private val accountService             = runtimeContext.newAccountDataService
  protected val authService: AuthService = runtimeContext.newAuthService

  lazy val routes: Route = pathPrefix("_matrix" / "client" / "r0" / "user") {
    path(Segment / "account_data" / Segment) { (mxUserId, dataType) =>
      requireAuth { credentials =>
        get {
          if (credentials.user.mxUserId == mxUserId) {
            accountService
              .getAccountData(credentials.account, dataType)
              .map {
                case Some(data) => complete(HttpEntity(ContentTypes.`application/json`, data.eventContent.noSpaces))
                case None       => complete(StatusCodes.NotFound)
              }
              .recover(failure => reject(ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage)))))
              .get
          } else {
            reject(
              server.ApiErrorRejection(
                ApiErrors.ForbiddenError(Some(s"API path userId '$mxUserId' doesn't match authorized user id"))
              )
            )
          }
        } ~
          put {
            entity(as[Json]) { jsonValue =>
              logger.debug(s"API CALL - put account_data $jsonValue")
              if (credentials.user.mxUserId == mxUserId) {
                accountService
                  .storeAccountData(credentials.account, dataType, jsonValue)
                  .map(_ => complete(()))
                  .recover(failure =>
                    reject(server.ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage))))
                  )
                  .get
              } else {
                reject(
                  server.ApiErrorRejection(
                    ApiErrors.ForbiddenError(Some(s"API path userId '$mxUserId' doesn't match authorized user id"))
                  )
                )
              }
            }
          }
      }
    }
  }
}
