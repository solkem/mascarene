/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.server.client.auth

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.StatusCodes.BadRequest
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Directive1, Route}
import com.google.common.util.concurrent.RateLimiter
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.Printer
import io.circe.generic.auto._
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.matrix.server
import org.mascarene.homeserver.matrix.server.{ApiErrorRejection, client}
import org.mascarene.homeserver.services.AuthService
import org.mascarene.matrix.client.r0.model.auth._
import org.mascarene.sdk.matrix.core.{ApiError, ApiErrors, ApiFailure}
import org.mascarene.utils.UserIdentifierUtils
import pureconfig._
import pureconfig.generic.auto._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.jdk.DurationConverters._
import scala.util.{Failure, Success, Try}

class AuthApiRoutes(system: ActorSystem[Nothing], clientApiRateLimiter: RateLimiter)(implicit
    val runtimeContext: RuntimeContext
) extends FailFastCirceSupport
    with AuthDirectives
    with ImplicitAskTimeOut
    with client.ThrottleDirective
    with LazyLogging {
  implicit private val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)
  private val throttleWaitTime =
    runtimeContext.config.getDuration("mascarene.server.client-api.throttle-wait-time").toScala
  private val matrixServerName           = runtimeContext.config.getString("mascarene.server.domain-name")
  protected val authService: AuthService = runtimeContext.newAuthService

  private def withRegistrationAuthFlow: Directive1[RegisterRequest] =
    entity(as[RegisterRequest]).flatMap { req =>
      onComplete(authService.checkAuthFlow("register", req)).flatMap {
        case Success(Left(request))       => provide(request)
        case Success(Right(authFlow))     => complete(StatusCodes.Unauthorized, authFlow)
        case Failure(failure: ApiFailure) => reject(ApiErrorRejection(failure.toApiError))
        case Failure(failure) =>
          reject(server.ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage))))
      }
    }

  private def withDeactivateAuthFlow: Directive1[DeactivateRequest] =
    entity(as[DeactivateRequest]).flatMap { req =>
      onComplete(authService.checkAuthFlow("deactivate", req)).flatMap {
        case Success(Left(request))       => provide(request)
        case Success(Right(authFlow))     => complete(StatusCodes.Unauthorized, authFlow)
        case Failure(failure: ApiFailure) => reject(server.ApiErrorRejection(failure.toApiError))
        case Failure(failure) =>
          reject(server.ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage))))
      }
    }

  private val loginAuthFlow = Try {
    ConfigSource
      .fromConfig(runtimeContext.config.getConfig("mascarene.matrix.interactive-auth.login"))
      .loadOrThrow[LoginAuthFlow]
  }

  lazy val routes: Route = pathPrefix("_matrix" / "client" / "r0") {
    path("login") {
      get {
        withThrottle(clientApiRateLimiter, throttleWaitTime) {
          loginAuthFlow match {
            case Failure(failure) => reject(server.ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage))))
            case Success(flow)    => complete(flow)
          }
        }
      } ~
        post {
          withThrottle(clientApiRateLimiter, throttleWaitTime) {
            extractUserAgent { userAgent =>
              entity(as[LoginRequest]) {
                case req @ LoginRequest("m.login.password", identifier, password, _, deviceId, initialDisplayName)
                    if identifier.contains("type") && identifier.contains("user") =>
                  logger.info("API CALL - POST login")
                  authService.loginAuthenticate(
                    identifier("user"),
                    password.getOrElse(""),
                    deviceId,
                    initialDisplayName,
                    userAgent
                  ) match {
                    case Success((_, user, device, authToken)) =>
                      complete(LoginResponse(user.mxUserId, authToken.encodedToken.get, device.mxDeviceId))
                    case Failure(failure: ApiFailure) =>
                      reject(server.ApiErrorRejection(failure.toApiError))
                    case Failure(failure) =>
                      reject(server.ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage))))
                  }
                case LoginRequest("m.login.password", _, _, _, _, _) =>
                  complete(
                    StatusCodes.Unauthorized,
                    ApiError("M_FORBIDDEN", Some("The provided authentication data was incorrect."))
                  )
                case LoginRequest(otherType, _, _, _, _, _) =>
                  complete(StatusCodes.BadRequest, ApiError("M_UNKNOWN", Some(s"Unsupported login type $otherType")))
              }
            }
          }
        }
    } ~
      path("logout") {
        requireAuth { credentials =>
          Future { authService.logout(credentials.device.deviceId) }
          complete(())
        }
      } ~
      path("logout" / "all") {
        requireAuth { credentials =>
          Future { authService.logoutAll(credentials.user.userId) }
          complete(())
        }
      } ~
      path("account" / "whoami") {
        withThrottle(clientApiRateLimiter, throttleWaitTime) {
          requireAuth { credentials => complete(WhoAmIResponse(credentials.user.mxUserId)) }
        }
      } ~
      path("register") {
        post {
          withThrottle(clientApiRateLimiter, throttleWaitTime) {
            parameters("kind".as[String].?) {
              case kind: Option[String] =>
                extractUserAgent { userAgent =>
                  withRegistrationAuthFlow { req =>
                    kind.getOrElse("user") match {
                      case "user" | "guest" =>
                        authService.registerUser(kind, req, userAgent) match {
                          case Success(resp) => complete(resp)
                          case Failure(failure: ApiFailure) =>
                            complete(BadRequest, failure.toApiError)
                          case Failure(failure) =>
                            reject(server.ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage))))
                        }
                      case otherKind =>
                        reject(
                          server.ApiErrorRejection(
                            ApiErrors
                              .Unrecognized(
                                Some(s"Unrecognized user kind '$otherKind'. Must be one of: ['guest', 'user']")
                              )
                          )
                        )
                    }
                  }
                }
            }
          }
        }
      } ~
      path("deactivate") {
        post {
          withThrottle(clientApiRateLimiter, throttleWaitTime) {
            withDeactivateAuthFlow { req =>
              requireAuth { credentials =>
                logger.info("API CALL - POST deactivate")
                authService.deactivateAccount(credentials.account) match {
                  case Success(_) => complete(DeactivateResponse(id_server_unbind_result = "no-support"))
                  case Failure(f) =>
                    reject(
                      server.ApiErrorRejection(
                        ApiErrors
                          .InternalError(Some(s"Failed to deactivate user: ${f.getMessage}"))
                      )
                    )
                }
              }
            }
          }
        }
      } ~
      path("register" / "available") {
        get {
          withThrottle(clientApiRateLimiter, throttleWaitTime) {
            parameter("username") { userName =>
              UserIdentifierUtils.build(userName, matrixServerName) match {
                case Failure(f) =>
                  reject(server.ApiErrorRejection(ApiError("M_INVALID_USERNAME", Some(f.getMessage))))
                case Success(mxId) =>
                  authService.isUserNameAvailable(mxId.toString) match {
                    case Success(true) => complete(Map("available" -> true))
                    case Success(false) =>
                      reject(
                        server.ApiErrorRejection(ApiError("M_USER_IN_USE", Some("Desired user ID is already taken.")))
                      )
                    case Failure(f) =>
                      reject(
                        server.ApiErrorRejection(
                          ApiErrors.InternalError(Some(s"Couldn't check username availability: ${f.getMessage}"))
                        )
                      )
                  }
              }
            }
          }
        }
      }
  }
}
