/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http.ServerBinding
import com.google.common.net.HostAndPort
import com.typesafe.config.Config
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import com.typesafe.scalalogging.LazyLogging
import org.mascarene.homeserver.internal.auth.AuthSessionCache
import org.mascarene.homeserver.internal.federation.FederationCluster
import org.mascarene.homeserver.internal.repository.DbContext
import org.mascarene.homeserver.internal.rooms.{EventStreamSequence, RoomCluster}

object MascareneGuard extends FailFastCirceSupport with LazyLogging {
  sealed trait Message
  private final case class StartFailed(cause: Throwable)   extends Message
  private final case class Started(binding: ServerBinding) extends Message
  case object Stop                                         extends Message

  def apply(config: Config, dbContext: DbContext): Behavior[Message] =
    Behaviors.setup { ctx =>
      implicit val runtimeContext: RuntimeContext =
        new RuntimeContext(ctx.system, config, dbContext)

      val eventStreamSequence = ctx.spawn(EventStreamSequence.apply, "EventStreamSequence")
      runtimeContext.actorRegistry.put("EventStreamSequence", eventStreamSequence.unsafeUpcast)
      val roomCluster = ctx.spawn(RoomCluster.apply(runtimeContext), "RoomCluster")
      runtimeContext.actorRegistry.put("RoomCluster", roomCluster.unsafeUpcast)
      val federationCluster = ctx.spawn(FederationCluster.apply(runtimeContext), "FederationCluster")
      runtimeContext.actorRegistry.put("FederationCluster", federationCluster.unsafeUpcast)
      runtimeContext.actorRegistry
        .put("AuthSessionCache", ctx.spawn(AuthSessionCache.apply, "AuthSessionCache").unsafeUpcast)

      import pureconfig._
      import pureconfig.generic.auto._
      val serverConfig = ConfigSource
        .fromConfig(runtimeContext.config.getConfig("mascarene.server"))
        .loadOrThrow[ServerConfig]

      val servers = serverConfig.listeners.map { listenerConfig =>
        val binding = HostAndPort.fromString(listenerConfig.bindAddress)
        ctx.log.info(s"Spawning mascarene server at ${listenerConfig.bindAddress}")
        ctx.spawn(
          MascareneServer(binding.getHost, binding.getPort, listenerConfig.roles),
          s"MascareneServer@${listenerConfig.bindAddress}"
        )
      }

      Behaviors
        .receiveMessagePartial[Message] {
          case Stop =>
            ctx.log.info(
              "Stopping Mascarene"
            )
            servers.foreach(_ ! MascareneServer.Stop)
            Behaviors.stopped
        }
    }
}
