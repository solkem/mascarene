package org.mascarene.homeserver.internal.model

import io.circe.Json

import java.time.Instant
import java.util.UUID

case class ApiTransaction(
    transactionId: UUID,
    path: String,
    txnId: String,
    authTokenId: UUID,
    responseContent: Json,
    createdAt: Instant
)
