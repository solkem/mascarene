/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.rooms

import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors, Routers}
import akka.actor.typed.{Behavior, SupervisorStrategy}
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity}
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.internal.rooms.RoomAgent.TypeKey

import scala.concurrent.ExecutionContextExecutor

object RoomCluster {
  def apply(implicit runtimeContext: RuntimeContext): Behavior[RoomClusterCommand] =
    Behaviors.setup { ctx => new RoomCluster(ctx) }
}

class RoomCluster(
    context: ActorContext[RoomClusterCommand]
)(implicit val runtimeContext: RuntimeContext)
    extends AbstractBehavior[RoomClusterCommand](context)
    with ImplicitAskTimeOut {
  implicit val ec: ExecutionContextExecutor = context.executionContext
  private val sharding                      = ClusterSharding(context.system)

  private val workerPoolSize = runtimeContext.config.getInt("mascarene.internal.room-worker-pool-size")
  private val pool = Routers.pool(workerPoolSize)(
    Behaviors.supervise(RoomWorker(runtimeContext)).onFailure[Exception](SupervisorStrategy.restart)
  )
  private val workerRouter = context.spawn(pool, "room-worker-pool")

  sharding.init(Entity(TypeKey) { entityContext =>
    RoomAgent(entityContext.entityId, runtimeContext, workerRouter)
  })

  override def onMessage(msg: RoomClusterCommand): Behavior[RoomClusterCommand] = {
    msg match {
      case GetRoomAgent(room, replyTo) =>
        replyTo ! GotRoomAgent(sharding.entityRefFor(RoomAgent.TypeKey, room.roomId.toString))
        this
    }
  }
}
