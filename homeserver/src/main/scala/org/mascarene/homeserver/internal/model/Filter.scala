package org.mascarene.homeserver.internal.model

import io.circe.parser.decode
import io.circe.generic.auto._, io.circe.syntax._
import org.mascarene.matrix.client.r0.model.filter.FilterDefinition

import java.time.Instant
import java.util.UUID

case class Filter(
    filterId: UUID,
    accountId: UUID,
    filterDefinition: Option[FilterDefinition] = None,
    createdAt: Instant,
    updatedAt: Option[Instant]
)
