/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.repository

import java.sql.PreparedStatement
import io.circe._
import io.circe.parser._
import io.circe.syntax._
import io.circe.generic.auto._
import io.circe.parser.parse
import org.mascarene.homeserver.RuntimeContext

trait JsonCodec {
  val runtimeContext: RuntimeContext
  import runtimeContext.dbContext._

  protected implicit val jsonDecoder: Decoder[Json] =
    decoder((index, row) => parse(row.getObject(index).toString).fold(error => throw error, json => json))
  protected implicit val jsonEncoder: Encoder[Json] = encoder[Json](
    java.sql.Types.OTHER,
    (index: Int, value: Json, row: PreparedStatement) =>
      row.setObject(index, value.asJson.noSpaces, java.sql.Types.OTHER)
  )

}
