package org.mascarene.homeserver.internal.federation

import org.mascarene.homeserver.internal.model.federation.ServerKey

case class FederationAgentState(keyId: String, serverKey: Option[ServerKey])
