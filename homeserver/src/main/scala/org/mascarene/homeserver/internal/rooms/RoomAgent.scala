/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.rooms

import java.util.UUID
import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, ActorSystem, Behavior}
import akka.cluster.sharding.typed.scaladsl.EntityTypeKey
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.internal.model.{Event, Room, StateSet}
//import io.circe.generic.auto._
import org.mascarene.homeserver.internal.repository._
import org.mascarene.sdk.matrix.core.ApiFailure
import com.typesafe.scalalogging.Logger
import org.mascarene.utils.LogUtils.time

import scala.util.{Failure, Success, Try}

object RoomAgent {
  val TypeKey: EntityTypeKey[RoomCommand] = EntityTypeKey[RoomCommand]("RoomAgent")

  def apply(
      entityId: String,
      runtimeContext: RuntimeContext,
      workerRouter: ActorRef[RoomWorkerCommand]
  ): Behavior[RoomCommand] =
    Behaviors.setup { ctx => new RoomAgent(entityId, ctx, workerRouter, runtimeContext) }
}

class RoomAgent(
    roomId: String,
    context: ActorContext[RoomCommand],
    workerRouter: ActorRef[RoomWorkerCommand],
    implicit val runtimeContext: RuntimeContext
) extends AbstractBehavior[RoomCommand](context)
    with ImplicitAskTimeOut {
  implicit val system: ActorSystem[Nothing] = context.system
  implicit val ec                           = system.executionContext
  private val roomRepo                      = new RoomRepo
  private val eventRepo                     = new EventRepo
  private val stateSetService               = runtimeContext.newStateSetService
  private val eventService                  = runtimeContext.newEventService

  context.log.debug("room agent init started")

  private[this] val room: Room = roomRepo.getRoomById(UUID.fromString(roomId)) match {
    case Success(Some(result)) => result
    case _ =>
      throw new ApiFailure("IO.MASCARENE.HS.INTERNAL_ERROR", s"Can't start room agent for unknown room $roomId")
  }
  private[this] var roomState = RoomState(room)
  private[this] val roomResolutionAlgorithm = room.version match {
    case "1"                   => V1RoomImpl(room)
    case "2" | "3" | "4" | "5" => V2RoomImpl(room)
    case _ =>
      throw new ApiFailure(
        "IO.MASCARENE.HS.INTERNAL_ERROR",
        s"Can't find resolution algorithm for room version ${room.version}"
      )
  }

  // Recover event resolution on startup
  eventRepo.getUnresolvedEvents(roomState.room.roomId).map { events =>
    val nbRecovered = events.map { event =>
      context.log.debug(s"Recovering event $event resolution")
      context.self ! ProcessEvent(event, None)
      1
    }.sum
    context.log.info(s"Room=${roomState.room.mxRoomId}: $nbRecovered events recovered for resolution")
  }
  eventRepo.getUnProcessedEvents(roomState.room.roomId).map { events =>
    val nbRecovered = events.map { event =>
      context.log.debug(s"Recovering event $event process")
      workerRouter ! CastStateEventEffects(roomState.room, event)
      1
    }.sum
    context.log.info(s"Room=${roomState.room.mxRoomId}: $nbRecovered events recovered for side effect process")
  }
  context.log.debug("room agent init completed")

  override def onMessage(msg: RoomCommand): Behavior[RoomCommand] = {
    msg match {
      case GetRoomState(replyTo) =>
        replyTo ! RoomStateResponse(roomState)
        Behaviors.same
      case ProcessEvent(event, replyTo) =>
        val (resolvedEvent, stateSetTry) = resolveStateEvent(event)
        stateSetTry
          .map { resolvedStateSet =>
            if (!resolvedEvent.rejected) {
              roomState = roomState.copy(lastResolvedEvent = Some(resolvedEvent))
              if (resolvedEvent.isStateEvent) {
                // Create a new room state version
                // and update room state
                context.log.debug(s"Update state with resolved event $resolvedEvent")
                val previousState = roomState.resolvedState
                roomState = roomState.copy(resolvedState = resolvedStateSet)
                val updateRoomTry = for {
                  newStateSetVersion <-
                    stateSetService.createNewVersion(roomState.room, resolvedStateSet, previousState)
                  updatedRoom <- roomRepo.updateLastStateSetVersion(roomState.room.roomId, newStateSetVersion)
                } yield updatedRoom
                updateRoomTry match {
                  case Success(updatedRoom) =>
                    roomState = roomState.copy(room = updatedRoom)
                    context.log.debug(
                      s"room ${room.mxRoomId} state updated to lastEvent=${roomState.lastResolvedEvent.get.mxEventId}"
                    )
                    // Cast state change side effects
                    workerRouter ! CastStateEventEffects(roomState.room, resolvedEvent)
                  case Failure(f) => context.log.warn(s"Room state version couldn't be persisted: ${f.getMessage}")
                }
              }
            }
          }
          .recover { f =>
            context.log.warn(s"Event resolution failed for event $resolvedEvent: ${f.getMessage}")
            context.log.debug("detail", f)
          }
        runtimeContext.eventStateSetCache.put(resolvedEvent.eventId, roomState.resolvedState)
        //apply current (or new) stateSetVersion to event
        eventRepo.updateEventStateSetVersion(Set(resolvedEvent.eventId), roomState.room.lastStatesetVersion)
        eventService.updateEventProcessed(resolvedEvent.eventId, processed = true)
        replyTo.foreach(_ ! EventProcessed(resolvedEvent))
        Behaviors.same
    }
  }

  private def resolveStateEvent(event: Event): (Event, Try[StateSet]) = {
    time(Logger(context.log), "event resolution time") {
      val (resolvedEvent, stateSetResult) = roomResolutionAlgorithm.resolve(event)
      eventService.updateEventResolved(resolvedEvent.eventId, resolved = true)
      (resolvedEvent, stateSetResult)
    }
  }
}
