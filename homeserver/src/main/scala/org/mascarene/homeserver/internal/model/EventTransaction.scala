package org.mascarene.homeserver.internal.model

import java.util.UUID

case class EventTransaction(eventId: UUID, transactionId: UUID)
