package org.mascarene.homeserver.internal.rooms

import org.mascarene.homeserver.internal.model.Event

import java.util.UUID
import scala.util.Try

sealed trait RoomResponse
case class RoomStateResponse(roomState: RoomState) extends RoomResponse
case class EventPosted(eventId: Try[UUID])         extends RoomResponse
case class EventProcessed(event: Event)            extends RoomResponse
