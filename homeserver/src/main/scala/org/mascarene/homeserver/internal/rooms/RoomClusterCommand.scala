package org.mascarene.homeserver.internal.rooms

import akka.actor.typed.ActorRef
import org.mascarene.homeserver.internal.model.Room

sealed trait RoomClusterCommand
case class GetRoomAgent(room: Room, replyTo: ActorRef[GotRoomAgent]) extends RoomClusterCommand
