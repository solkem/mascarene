/*
 * Mascarene
 * Copyright (C) 2020 mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.repository

import org.mascarene.homeserver.RuntimeContext

import java.sql.{PreparedStatement, Timestamp}
import java.time.Instant

trait InstantCodec {
  val runtimeContext: RuntimeContext
  import runtimeContext.dbContext._

  protected implicit val instantDecoder: Decoder[Instant] =
    decoder((index, row) => row.getObject(index, classOf[Timestamp]).toInstant)
  protected implicit val instantEncoder: Encoder[Instant] = encoder[Instant](
    java.sql.Types.TIMESTAMP,
    (index: Int, value: Instant, row: PreparedStatement) =>
      row.setObject(index, Timestamp.from(value), java.sql.Types.TIMESTAMP)
  )

}
