package org.mascarene.homeserver.internal.model

import java.time.Instant
import java.util.UUID

case class Account(
    accountId: UUID,
    userId: UUID,
    passwordHash: Option[String],
    kind: String,
    deactivated: Boolean,
    createdAt: Instant,
    updatedAt: Option[Instant]
)
