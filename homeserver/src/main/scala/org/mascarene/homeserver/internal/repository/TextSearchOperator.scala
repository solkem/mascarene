package org.mascarene.homeserver.internal.repository

import org.mascarene.homeserver.RuntimeContext

trait TextSearchOperator {
  val runtimeContext: RuntimeContext
  import runtimeContext.dbContext._

  protected val toTsVector = quote { (doc: String) => infix"to_tsvector($doc)".as[String] }
  protected val toTsQuery  = quote { (term: String) => infix"to_tsquery($term)".as[String] }

  implicit class TsQuotes(left: String) {
    def @@(right: String) = quote(infix"$left @@ $right".as[Boolean])
  }
}
