/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.rooms

import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors}
import akka.actor.typed.Behavior
import io.circe.generic.auto._
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.internal.model.{Event, EventContent, EventTypes, Room}
import org.mascarene.matrix.client.r0.model.events.{
  MemberEventContent,
  RoomAliasEventContent,
  RoomCanonicalAliasEventContent,
  RoomNameEventContent,
  RoomTopicEventContent
}

object RoomWorker {
  def apply(implicit runtimeContext: RuntimeContext): Behavior[RoomWorkerCommand] =
    Behaviors.setup { context => new RoomWorker(context) }
}

class RoomWorker(
    context: ActorContext[RoomWorkerCommand]
)(implicit val runtimeContext: RuntimeContext)
    extends AbstractBehavior[RoomWorkerCommand](context)
    with ImplicitAskTimeOut {
  private val roomService  = runtimeContext.newRoomService
  private val authService  = runtimeContext.newAuthService
  private val eventService = runtimeContext.newEventService

  override def onMessage(msg: RoomWorkerCommand): Behavior[RoomWorkerCommand] = {
    msg match {
      case CastStateEventEffects(room, stateEvent) =>
        castStateEventEffects(room, stateEvent)
        Behaviors.same
      case CastRoomMemberEffects(room, roomMemberEvent, eventContent) =>
        castRoomMemberEffects(room, roomMemberEvent, eventContent)
        Behaviors.same
      case CastRoomNameEffects(room, roomNameEvent, eventContent) =>
        castRoomNameEffects(room, roomNameEvent, eventContent)
        Behaviors.same
      case CastRoomAliasEffects(room, roomAliasEvent, eventContent) =>
        castRoomAliasEffects(room, roomAliasEvent, eventContent)
        Behaviors.same
      case CastRoomTopicEffects(room, roomAliasEvent, eventContent) =>
        castRoomTopicEffects(room, roomAliasEvent, eventContent)
        Behaviors.same
      case UpdateEventProcessed(event) =>
        eventService.updateEventProcessed(event.eventId, processed = true)
        Behaviors.same
      case CastRoomIndexTerm(room, eventType, term) =>
        roomService.indexRoomTerm(room, eventType, term)
        Behaviors.same
    }

  }

  private def castStateEventEffects(room: Room, stateEvent: Event): Unit = {
    eventService.getEventContent(stateEvent).foreach { eventContent =>
      stateEvent.eventType match {
        case EventTypes.M_ROOM_MEMBER =>
          eventContent.foreach(content => context.self ! CastRoomMemberEffects(room, stateEvent, content))
        case EventTypes.M_ROOM_ALIASES =>
          eventContent.foreach(content => context.self ! CastRoomAliasEffects(room, stateEvent, content))
        case EventTypes.M_ROOM_NAME =>
          eventContent.foreach(content => context.self ! CastRoomNameEffects(room, stateEvent, content))
        case EventTypes.M_ROOM_TOPIC =>
          eventContent.foreach(content => context.self ! CastRoomTopicEffects(room, stateEvent, content))
        case EventTypes.M_ROOM_CANONICAL_ALIAS =>
          eventContent.foreach(content => context.self ! CastRoomCanonicalAliasEffects(room, stateEvent, content))
        case _ =>
          context.log.warn(s"No side effect for event type `${stateEvent.eventType}`")
      }
    }
  }

  private def castRoomMemberEffects(
      room: Room,
      roomMemberEvent: Event,
      eventContent: EventContent
  ): Unit = {
    context.log.debug(s"Casting RoomMember effects for event ${roomMemberEvent.mxEventId}")
    val stateKey = roomMemberEvent.stateKey.get
    val contentTry = for {
      memberEventContent <- eventContent.content.get.as[MemberEventContent].toTry
      user               <- authService.getOrCreateUser(stateKey)
    } yield (memberEventContent, user)
    contentTry
      .flatMap {
        case (memberEventContent, user) =>
          roomService.createOrUpdateMembership(room, user, roomMemberEvent, memberEventContent.membership)
      }
      .recover(f => context.log.warn(s"Failed to cast m.room.member effects: ${f.getMessage}"))
  }

  private def castRoomAliasEffects(
      room: Room,
      roomAliasEvent: Event,
      eventContent: EventContent
  ): Unit = {
    val serverDomain = roomAliasEvent.stateKey.get
    val contentTry = for {
      roomAliasEventContent <- eventContent.content.get.as[RoomAliasEventContent].toTry
      user                  <- authService.getUser(roomAliasEvent.senderId)
    } yield (roomAliasEventContent, user)
    contentTry
      .map {
        case (roomAliasEventContent, creator) =>
          roomAliasEventContent.aliases.foreach(alias =>
            roomService.createRoomAlias(room, creator.get, alias, serverDomain)
          )
          context.self ! CastRoomIndexTerm(room, EventTypes.M_ROOM_ALIASES, roomAliasEventContent.aliases.mkString(","))
      }
      .recover(f => context.log.warn(s"Failed to cast m.room.aliases effects: ${f.getMessage}"))
  }

  private def castRoomNameEffects(
      room: Room,
      roomNameEvent: Event,
      eventContent: EventContent
  ): Unit = {
    val contentTry = eventContent.content.get.as[RoomNameEventContent].toTry
    contentTry
      .map { roomNameEventContent =>
        context.self ! CastRoomIndexTerm(room, EventTypes.M_ROOM_NAME, roomNameEventContent.name)
      }
      .recover(f => context.log.warn(s"Failed to cast ${EventTypes.M_ROOM_NAME} effects: ${f.getMessage}"))
  }

  private def castRoomTopicEffects(
      room: Room,
      roomTopicEvent: Event,
      eventContent: EventContent
  ): Unit = {
    val contentTry = eventContent.content.get.as[RoomTopicEventContent].toTry
    contentTry
      .map { roomTopicEventContent =>
        context.self ! CastRoomIndexTerm(room, EventTypes.M_ROOM_TOPIC, roomTopicEventContent.topic)
      }
      .recover(f => context.log.warn(s"Failed to cast ${EventTypes.M_ROOM_TOPIC} effects: ${f.getMessage}"))
  }

  private def castRoomCanonicalAliasEffects(
      room: Room,
      roomCanonicalAliasEvent: Event,
      eventContent: EventContent
  ): Unit = {
    val contentTry = eventContent.content.get.as[RoomCanonicalAliasEventContent].toTry
    contentTry
      .map { roomCanonicalAliasEventContent =>
        context.self ! CastRoomIndexTerm(room, EventTypes.M_ROOM_CANONICAL_ALIAS, roomCanonicalAliasEventContent.alias)
      }
      .recover(f => context.log.warn(s"Failed to cast ${EventTypes.M_ROOM_CANONICAL_ALIAS} effects: ${f.getMessage}"))
  }
}
