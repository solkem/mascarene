/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.auth

import java.time.Instant
import java.util.concurrent.TimeUnit
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import akka.cluster.ddata.Replicator.{ReadLocal, WriteLocal}
import akka.cluster.ddata.typed.scaladsl.Replicator._
import akka.cluster.ddata.typed.scaladsl.{DistributedData, Replicator}
import akka.cluster.ddata.{LWWMap, LWWMapKey, SelfUniqueAddress}
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.matrix.client.r0.model.auth.AuthFlow
import org.mascarene.utils.Codecs

import scala.concurrent.duration.FiniteDuration
import scala.util.Random

case class CachedAuthSession(authFlow: AuthFlow, expiresAt: Instant)
object AuthSessionCache {
  private sealed trait InternalCommand extends AuthSessionCacheCommand
  private case class InternalSubscribeResponse(chg: Replicator.SubscribeResponse[LWWMap[String, CachedAuthSession]])
      extends InternalCommand
  private case class InternalUpdateResponse(rsp: UpdateResponse[LWWMap[String, CachedAuthSession]])
      extends InternalCommand
  private case class InternalGetResponse(
      key: String,
      replyTo: ActorRef[AuthSessionResponse],
      rsp: GetResponse[LWWMap[String, CachedAuthSession]]
  ) extends InternalCommand
  private case class InternalInvalidateExpired(rsp: GetResponse[LWWMap[String, CachedAuthSession]])
      extends InternalCommand
  private case object Tick extends AuthSessionCacheCommand

  def apply(implicit runtimeContext: RuntimeContext): Behavior[AuthSessionCacheCommand] =
    Behaviors.setup { context =>
      implicit val node: SelfUniqueAddress = DistributedData(context.system).selfUniqueAddress

      DistributedData.withReplicatorMessageAdapter[AuthSessionCacheCommand, LWWMap[String, CachedAuthSession]] {
        replicatorAdapter =>
          Behaviors.withTimers[AuthSessionCacheCommand] { timers =>
            val cacheExpireDelay =
              runtimeContext.config.getDuration("mascarene.server.auth-session-cache.expire-delay")
            val cacheMaximumSize = runtimeContext.config.getLong("mascarene.server.auth-session-cache.maximum-size")
            val authSessionsKey  = LWWMapKey[String, CachedAuthSession]("authSessions")
            val tickDelay =
              FiniteDuration(Math.abs(new Random().nextLong() % cacheExpireDelay.toMinutes) + 1, TimeUnit.MINUTES)
            context.log.debug(s"Expires tick interval: $tickDelay")
            timers.startTimerWithFixedDelay(Tick, tickDelay)

            Behaviors.receiveMessage {
              case Tick =>
                replicatorAdapter.askGet(
                  askReplyTo => Get(authSessionsKey, ReadLocal, askReplyTo),
                  rsp => InternalInvalidateExpired(rsp)
                )
                Behaviors.same
              case PutAuthSession(authFlow, replyTo) =>
                val session             = Codecs.genSessionId()
                val authFlowWithSession = authFlow.copy(session = Some(session))
                replicatorAdapter.askUpdate(
                  askReplyTo =>
                    Update(authSessionsKey, LWWMap.empty[String, CachedAuthSession], WriteLocal, askReplyTo)(
                      _ :+ (session -> CachedAuthSession(authFlowWithSession, Instant.now().plus(cacheExpireDelay)))
                    ),
                  InternalUpdateResponse.apply
                )
                replyTo ! AuthSessionStored(authFlowWithSession)
                Behaviors.same
              case InvalidateAuthSession(authSession) =>
                authSession.session.foreach { session =>
                  replicatorAdapter.askUpdate(
                    askReplyTo =>
                      Update(authSessionsKey, LWWMap.empty[String, CachedAuthSession], WriteLocal, askReplyTo)(
                        _.remove(node, session)
                      ),
                    InternalUpdateResponse.apply
                  )
                }
                Behaviors.same
              case GetAuthSession(session, replyTo) =>
                replicatorAdapter.askGet(
                  askReplyTo => Get(authSessionsKey, ReadLocal, askReplyTo),
                  rsp => InternalGetResponse(session, replyTo, rsp)
                )
                Behaviors.same
              case UpdateAuthSession(authSession) =>
                authSession.session.foreach { session =>
                  replicatorAdapter.askUpdate(
                    askReplyTo =>
                      Update(authSessionsKey, LWWMap.empty[String, CachedAuthSession], WriteLocal, askReplyTo)(
                        _ :+ (session -> CachedAuthSession(authSession, Instant.now().plus(cacheExpireDelay)))
                      ),
                    InternalUpdateResponse.apply
                  )
                }
                Behaviors.same
              case InternalGetResponse(session, replyTo, g @ GetSuccess(_)) =>
                g.dataValue
                  .get(session)
                  .map { cachedAuthSession =>
                    if (cachedAuthSession.expiresAt.isBefore(Instant.now())) {
                      replyTo ! AuthSessionResponse(None)
                      context.log.debug(s"Invalidating expired auth session ${cachedAuthSession.authFlow.session}")
                      context.self ! InvalidateAuthSession(cachedAuthSession.authFlow)
                    } else
                      replyTo ! AuthSessionResponse(Some(cachedAuthSession.authFlow))
                  }
                  .getOrElse(replyTo ! AuthSessionResponse(None))
                Behaviors.same
              case InternalGetResponse(_, replyTo, _: NotFound[_]) =>
                replyTo ! AuthSessionResponse(None)
                Behaviors.same
              case InternalInvalidateExpired(g @ GetSuccess(_)) =>
                val values = g.dataValue.entries.values
                if (values.size > cacheMaximumSize) {
                  g.dataValue.entries.values.toList
                    .sortBy(_.expiresAt)
                    .take((values.size - cacheMaximumSize).toInt)
                    .foreach { cached =>
                      context.log.debug(s"Invalidating oldest auth session ${cached.authFlow.session}")
                      context.self ! InvalidateAuthSession(cached.authFlow)
                    }
                }
                values
                  .filter(_.expiresAt.isBefore(Instant.now()))
                  .foreach { cached =>
                    context.log.debug(s"Invalidating expired auth session ${cached.authFlow.session}")
                    context.self ! InvalidateAuthSession(cached.authFlow)
                  }
                Behaviors.same
              case InternalInvalidateExpired(_: NotFound[_]) =>
                Behaviors.same
              case _: InternalGetResponse    => Behaviors.same // ok
              case _: InternalUpdateResponse => Behaviors.same // ok
            }

          }
      }
    }
}
