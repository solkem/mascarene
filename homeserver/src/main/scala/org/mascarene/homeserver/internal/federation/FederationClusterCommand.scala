package org.mascarene.homeserver.internal.federation

import akka.actor.typed.ActorRef

sealed trait FederationClusterCommand
case class GetFederationAgent(authority: String, replyTo: ActorRef[GotFederationAgent]) extends FederationClusterCommand
