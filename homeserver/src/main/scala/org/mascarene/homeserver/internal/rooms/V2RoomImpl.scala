/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.rooms

import java.util.UUID
import cats.data.EitherT
import cats.implicits._
import com.typesafe.scalalogging.LazyLogging
import io.circe.generic.auto._
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.model.{Event, EventTypes, Room, StateSet}
import org.mascarene.homeserver.internal.repository.AuthRepo
import org.mascarene.homeserver.internal.rooms.validators.{
  EventAuthValidation,
  EventValidator,
  MRoomAliasesEventValidator,
  MRoomCreateEventValidator,
  MRoomMemberEventValidator,
  MRoomPowerLevelsEventValidator,
  MRoomRedactionEventValidator,
  MRoomThirdPartyInviteEventValidator
}
import org.mascarene.matrix.client.r0.model.events.MemberEventContent

import scala.concurrent.ExecutionContext.Implicits.global
import scala.collection.immutable.VectorMap
import scala.collection.mutable
import scala.concurrent.{Await, Future}
import scala.jdk.DurationConverters._
import scala.util.{Failure, Success, Try}

class V2RoomImpl(room: Room)(implicit runtimeContext: RuntimeContext) extends RoomVersionImpl with LazyLogging {

  private[this] val stateSetService = runtimeContext.newStateSetService
  private[this] val authRepo        = new AuthRepo

  private[this] val eventValidator                      = new EventValidator
  private[this] val mRoomCreateEventValidator           = new MRoomCreateEventValidator(room, eventRepo, authRepo)
  private[this] val mRoomAliasesEventValidator          = new MRoomAliasesEventValidator
  private[this] val mRoomMemberEventValidator           = new MRoomMemberEventValidator
  private[this] val mRoomThirdPartyInviteEventValidator = new MRoomThirdPartyInviteEventValidator
  private[this] val mRoomPowerLevelsEventValidator      = new MRoomPowerLevelsEventValidator
  private[this] val mRoomRedactionEventValidator        = new MRoomRedactionEventValidator

  private val defaultAwaitTimeout =
    runtimeContext.config.getDuration("mascarene.internal.default-await-timeout").toScala

  /**
    * Resolve event state according to https://matrix.uhoreg.ca/stateres/reloaded.html
    *
    * @param resolvedEvent event to resolve
    * @return a tuple containing the updated event (in cas of rejection) and the resolved state set
    */
  override def resolve(resolvedEvent: Event): (Event, Try[StateSet]) =
    Try[(Event, Try[StateSet])] {
      val parentEvents = eventRepo.getParentEventsWithCache(resolvedEvent.eventId)
      //State sets is composed of prev events state set + event state set if the event is a state event
      val eventStateSet: StateSet =
        if (resolvedEvent.isStateEvent)
          StateSet(Map(resolvedEvent.eventType -> Map(resolvedEvent.stateKey.get -> resolvedEvent)))
        else StateSet()
      val stateSets: Set[StateSet] = parentEvents.get
        .map(parent => stateSetService.loadStateSetForEventWithCache(parent))
        .filter(_.nonEmpty)
        .map(_.get) + eventStateSet

      val (unconflictedStateMap, conflictedSet): (StateSet, Set[Event]) = calculateConflicts(stateSets)
      val fullConflictedSet: Set[Event]                                 = conflictedSet.union(authDifference(stateSets).get)
      val conflictedPowerEvents                                         = fullConflictedSet.filter(event => isPowerEvent(event).get)
      val conflictedPowerEventsWithAuth =
        conflictedPowerEvents.union(fullAuthChain(conflictedPowerEvents).get.intersect(fullConflictedSet))
      val sortedPowerEvents = revTopPowSort(conflictedPowerEventsWithAuth)
      val partialResolvedState: StateSet =
        iterativeAuthChecks(sortedPowerEvents.getOrElse(Seq.empty), unconflictedStateMap)
      val otherConflictedEvents: Set[Event] = fullConflictedSet.diff(conflictedPowerEventsWithAuth)
      val resolvedPowerLevel                = partialResolvedState.getEvent(EventTypes.M_ROOM_POWER_LEVELS, "")
      val sortedOtherEvents                 = resolvedPowerLevel.map { pl => otherConflictedEvents.toSeq.sorted(mainLineOrder(pl)) }
      val nearlyFinalState: StateSet        = iterativeAuthChecks(sortedOtherEvents.getOrElse(Seq.empty), partialResolvedState)
      val resolution: StateSet              = unconflictedStateMap ++ nearlyFinalState

      eventRepo
        .isEventRejected(resolvedEvent.eventId)
        .map {
          case Some(true) =>
            (resolvedEvent.copy(rejected = true), Success(resolution))
          case _ =>
            (resolvedEvent, Success(resolution))
        }
        .get
    }.recover { f =>
      eventRepo.rejectEvent(resolvedEvent.eventId, s"resolution failed with unexpected exception: ${f.getMessage}")
      (resolvedEvent.copy(rejected = true), Failure(f))
    }.get

  private def mainLineOrder(pl: Event): Ordering[Event] = {
    def mainLine(event: Event): Seq[Event] = {
      Seq.unfold[Event, Event](event)(e => findPL(e).map(plEvent => (e, plEvent)))
    }

    val mainlineMap: Map[Event, Int] = mainLine(pl).zip(LazyList.from(0, -1)).toMap

    def mainLineDepth(event: Event): Option[Int] = {
      val mayBeLd = mainLine(event).map(e => mainlineMap.get(e))
      if (mayBeLd.isEmpty)
        None
      else mayBeLd.head
    }

    (e1: Event, e2: Event) => {
      val comp1 = for {
        d1 <- mainLineDepth(e1)
        d2 <- mainLineDepth(e2)
      } yield d1 - d2
      comp1.getOrElse {
        val comp2 = e1.originServerTs.compareTo(e2.originServerTs)
        val comp3 = e1.mxEventId.compare(e2.mxEventId)
        if (comp2 == 0)
          comp3
        else
          comp2
      }
    }
  }

  def findPL(event: Event): Option[Event] =
    eventRepo.getAuthEventsWithCache(event.eventId).get.find(_.eventType == EventTypes.M_ROOM_POWER_LEVELS)

  /**
    * Power events are defined as  a subset of state events which specifically have the power to add or remove
    * abilities from another user. These are:
    *  - Setting room power levels: m.room.power_levels where the state_key equals ""
    *  - Setting room join rules: m.room.join_rules where the state_key equals ""
    *  - Kicking a user: m.room.member with "leave" content but where the sender (the admin/mod) does not match
    * the state_key (the kicked user)
    *  - Banning a user: m.room.member with "ban" content but where the sender (the admin/mod) does not match the
    * state_key (the banned user)
    *
    * @param event
    * @return
    */
  private def isPowerEvent(event: Event): Try[Boolean] =
    Try {
      if (!event.isStateEvent)
        false
      else {
        event.eventType match {
          case EventTypes.M_ROOM_POWER_LEVELS if event.stateKey.contains("") => true
          case EventTypes.M_ROOM_JOIN_RULES if event.stateKey.contains("")   => true
          case EventTypes.M_ROOM_MEMBER =>
            val repoOps = for {
              sender        <- authRepo.getUserByIdWithCache(event.senderId).get
              c             <- eventRepo.getEventContent(event.eventId).get
              memberContent <- c.content.get.as[MemberEventContent].toOption
            } yield (sender, memberContent)
            val (sender, content) = repoOps.get
            sender.mxUserId != event.stateKey.get && (content.membership == "ban" || content.membership == "leave")
          case _ => false
        }
      }
    }

  /**
    * Find the power level of an event sender
    *
    * @param event to look for event power level
    * @return sender power level
    */
  private def findSenderPowerLevel(event: Event): Int = {
    def findPowerLevelOrCreateEvent(events: Set[Event]): Option[Event] = {
      events
        .find(e => e.eventType == EventTypes.M_ROOM_POWER_LEVELS)
        .orElse(events.find(e => e.eventType == EventTypes.M_ROOM_CREATE))
    }

    val res = for {
      sender     <- authRepo.getUserByIdWithCache(event.senderId)
      plOrCreate <- eventRepo.getAuthEventsWithCache(event.eventId).map(findPowerLevelOrCreateEvent)
      content    <- eventRepo.getEventContent(event.eventId)
    } yield (sender, plOrCreate, content)
    res.map {
      case (Some(user), Some(plOrCreate), content) =>
        plOrCreate.eventType match {
          case EventTypes.M_ROOM_CREATE => if (event.senderId == plOrCreate.senderId) 100 else 0
          case EventTypes.M_ROOM_POWER_LEVELS =>
            val plContent = PowerLevelsUtils.getPlContent(content.get)
            PowerLevelsUtils.findUserPowerLevel(user.mxUserId, plContent)
        }
      case _ => 0
    }
  } match {
    case Success(pl) => pl
    case Failure(f) =>
      logger.warn(s"Failed to get user power level for event ${event.eventId}. Cause: ${f.getMessage}")
      logger.debug("details", f)
      0
  }

  /**
    * Reverse ordering sort method
    * The reverse topological power ordering of a set of events is the lexicographically smallest topological
    * ordering based on the DAG formed by auth events. The reverse topological power ordering is ordered from
    * earliest event to latest. For comparing two topological orderings to determine which is the lexicographically
    * smallest, the following comparison relation on events is used: for events x and y, x < y if
    *  - x's sender has greater power level than y's sender, when looking at their respective auth_events; or
    *  - the senders have the same power level, but x's origin_server_ts is less than y's origin_server_ts; or
    *  - the senders have the same power level and the events have the same origin_server_ts, but x's event_id is
    * less than y's event_id.
    *
    * @param eventMap
    * @param incomingEdges
    * @return
    */
  private def sortedIncomingEdges(eventMap: Map[UUID, Event], incomingEdges: Map[UUID, Int]): VectorMap[UUID, Int] = {
    //logger.debug(s"incomingEdges: $incomingEdges, eventMap: $eventMap")
    val eventOrdering = new Ordering[Event] {
      override def compare(x: Event, y: Event): Int = {
        val comp1 = findSenderPowerLevel(x) - findSenderPowerLevel(y)
        val comp2 = x.originServerTs.compareTo(y.originServerTs)
        val comp3 = x.mxEventId.compare(y.mxEventId)
        if (comp1 != 0)
          comp1
        else {
          if (comp2 != 0)
            comp2
          else
            comp3
        }
      }
    }

    val outgoing = VectorMap() ++
      eventMap
        .filter {
          case (uuid, _) => incomingEdges.contains(uuid)
        }
        .values
        .toSeq
        .sorted(eventOrdering)
        .map(ev => ev.eventId -> incomingEdges(ev.eventId))
    //logger.debug(s"outgoing sorted edges: $outgoing")
    outgoing
  }

  /**
    * Reverse topological ordering of control events according to :
    *  - https://matrix.org/docs/guides/implementing-stateres
    *  - https://matrix.org/docs/spec/rooms/v2
    *
    * @param powerEvents list of power events to sort
    * @return list of sorted events
    */
  private def revTopPowSort(powerEvents: Set[Event]): Try[Seq[Event]] =
    Try {
      val incomingEdges: mutable.Map[UUID, Int] = mutable.Map.from(powerEvents.map(e => e.eventId -> 0))

      val powerEventsChain = powerEvents
        .flatMap { event => authChain(event).get }
      incomingEdges ++= powerEventsChain.groupMapReduce(_.eventId)(_ => 1)(_ + _)

      val eventsMap: Map[UUID, Event] = Map.from((powerEvents ++ powerEventsChain).map(e => e.eventId -> e))

      var outputEvents = mutable.Seq.empty[Event]
      while (incomingEdges.nonEmpty) {
        sortedIncomingEdges(eventsMap, incomingEdges.toMap).foreach {
          case (eventId, edgeCount) =>
            if (edgeCount == 0)
              outputEvents = outputEvents.prepended(eventsMap(eventId))
            eventRepo.getAuthEventsWithCache(eventId).map {
              _.map { authEvent => incomingEdges.update(authEvent.eventId, incomingEdges(authEvent.eventId) - 1) }
            }
            incomingEdges.remove(eventId)
        }
      }
      outputEvents.toSeq
    }

  private def calculateConflicts(stateSets: Set[StateSet]): (StateSet, Set[Event]) = {
    def eventsForKey(key: (String, String), stateSets: Set[StateSet]) =
      stateSets.map(stateSet => stateSet.getEvent(key)).filter(_.isDefined).map(_.get)

    // Extract unique (eventType, stateKey) from a set of state set
    val domain                             = stateSets.flatMap(_.keys)
    val fullStateMapList                   = domain.map(k => k -> eventsForKey(k, stateSets))
    val (unconflictedList, conflictedList) = fullStateMapList.partition { case (k, events) => events.size == 1 }
    val unconflictedStateMap: StateSet = unconflictedList
      .map { case (k, eventSet) => eventSet.head }
      .foldLeft(StateSet())((stateSet, event) => stateSet.updatedWith(event)) //updateStateSet(stateSet, event))
    val conflictedSet = Set.concat(conflictedList.flatMap(_._2))
    (unconflictedStateMap, conflictedSet)
  }

  private def fullAuthChain(events: Set[Event]): Try[Set[Event]] =
    Try {
      Set.from(events.flatMap(e => authChain(e).get))
    }

  /**
    * Given some state sets, the auth difference is calculated by first calculating the full auth chain for each
    * state set (that is, the union of the auth chains for the events in the state set) and taking every event that
    * doesn't appear in every auth chain.
    * In other words, if we take the full auth chains for the state sets, then the auth difference is their union
    * minus their intersection.
    *
    * @param stateSets
    * @return
    */
  private def authDifference(stateSets: Set[StateSet]) =
    Try {
      val fullAuthChains =
        stateSets
          .map(stateSet => stateSet.values)
          .map(events => fullAuthChain(Set.from(events)).get)
      val authChainsUnion: Set[Event] = Set.concat(fullAuthChains.flatten)
      val authChainsIntersection: Set[Event] =
        if (fullAuthChains.isEmpty) Set.empty else fullAuthChains.reduceLeft((acc, set) => acc.intersect(set))
      authChainsUnion.diff(authChainsIntersection)
    }

  private def authChain(event: Event): Try[Set[Event]] = {
    for {
      authEvents <- eventRepo.getAuthEventsWithCache(event.eventId)
      r <- Try {
        authEvents.flatMap(e => authChain(e).get)
      }
    } yield authEvents.union(r)
  }

  def iterativeAuthChecks(events: Seq[Event], stateSet: StateSet): StateSet = {
    events.foldLeft(stateSet) { (stateSetAcc, event) =>
      if (isAuthorized(event, stateSet)) {
        stateSetAcc.updatedWith(event)
      } else stateSetAcc
    }
  }

  def isAuthorized(e: Event, stateSet: StateSet): Boolean = {
    val augmentedStateSet = eventRepo.getAuthEventsWithCache(e.eventId).get.foldLeft(stateSet) { (stateSetAcc, e) =>
      val (eventType, stateKey) = (e.eventType, e.stateKey.get)
      if (stateSetAcc.exists(eventType, stateKey)) stateSetAcc else stateSetAcc.updatedWith(e)
    }
    checkAuthorization(e, augmentedStateSet)
  }

  /**
    * Event authorization checks according to https://matrix.org/docs/spec/rooms/v1#authorization-rules
    *
    * @param event    event to check authorizations
    * @param stateSet stateSet containing state events for this event state
    */
  private def checkAuthorization(event: Event, stateSet: StateSet): Boolean = {
    val validationResult: EitherT[Future, EventAuthValidation, Event] = event.eventType match {
      case EventTypes.M_ROOM_CREATE             => mRoomCreateEventValidator.validate(event)
      case EventTypes.M_ROOM_ALIASES            => mRoomAliasesEventValidator.validate(event, stateSet)
      case EventTypes.M_ROOM_MEMBER             => mRoomMemberEventValidator.validate(event, stateSet)
      case EventTypes.M_ROOM_THIRD_PARTY_INVITE => mRoomThirdPartyInviteEventValidator.validate(event, stateSet)
      case EventTypes.M_ROOM_POWER_LEVELS       => mRoomPowerLevelsEventValidator.validate(event, stateSet)
      case EventTypes.M_ROOM_REDACTION          => mRoomRedactionEventValidator.validate(event, stateSet)
      case _                                    => eventValidator.validateDefault(event, stateSet)
    }
    val validationFuture = validationResult.value.flatMap {
      case Right(_) => Future.successful(true)
      case Left(validationResult) =>
        logger.debug(s"$event rejected with cause: ${validationResult.rejectionCause}")
        Future.fromTry(eventRepo.rejectEvent(event.eventId, validationResult.rejectionCause)).map(_ => false)
    }
    Await.result(validationFuture, defaultAwaitTimeout)
  }
}

object V2RoomImpl {
  def apply(room: Room)(implicit runtimeContext: RuntimeContext) =
    new V2RoomImpl(room)
}
