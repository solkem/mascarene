/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.rooms.validators

import org.mascarene.utils.UserIdentifierUtils
import cats.data.EitherT
import cats.implicits._
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.model.{Event, StateSet}

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}
import scala.concurrent.ExecutionContext.Implicits.global

case class InvalidIdentifierFormat(message: String, event: Event) extends EventAuthValidation {
  override def rejectionCause: String = s"InvalidIdentifierFormat: $message"
}

class MRoomAliasesEventValidator(implicit runtimeContext: RuntimeContext) extends EventValidator {
  private def validateSenderDomainMatchesStateKey(event: Event): EitherT[Future, EventAuthValidation, Event] =
    EitherT {
      Future {
        val domainMatch = for {
          sender         <- authRepo.getUserByIdWithCache(event.senderId)
          userIdentifier <- UserIdentifierUtils.parse(sender.get.mxUserId)
          res <- Try {
            userIdentifier.domain == event.stateKey.get
          }
        } yield res
        domainMatch match {
          case Failure(f)     => Left(InvalidIdentifierFormat(f.getMessage, event))
          case Success(false) => Left(SenderDomainMismatchStateKey(event))
          case Success(true)  => Right(event)
        }
      }
    }
  def validate(event: Event, stateSet: StateSet): EitherT[Future, EventAuthValidation, Event] = {
    val f1 = validateAuthEvents(event, stateSet)
    val f2 = validateSenderDomainMatchesStateKey(event)
    for {
      _ <- f1
      _ <- f2
    } yield event
  }
}
