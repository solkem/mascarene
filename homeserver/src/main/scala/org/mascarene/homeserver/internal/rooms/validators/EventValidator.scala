/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.rooms.validators

import org.mascarene.homeserver.internal.repository.{AuthRepo, EventRepo}
import org.mascarene.homeserver.internal.rooms.PowerLevelsUtils
import cats.data.{EitherT, Validated, ValidatedNec}
import cats.implicits._
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.model.{Event, EventTypes, StateSet}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Success, Try}

case class ThirdPartyInviteSignatureMxIdMismatchStateKey(event: Event) extends EventAuthValidation {
  override def rejectionCause: String =
    s"ThirdPartyInviteSignatureMxIdMismatchStateKey: The third party invite signature mxId doesn't match event state key"
}
case class IncompatibleMembership(event: Event, senderMxId: String, currentMembership: String)
    extends EventAuthValidation {
  override def rejectionCause: String =
    s"IncompatibleMembership: event membership is incompatible with $senderMxId current membership: $currentMembership"
}
case class InvalidStateKeyContent(event: Event) extends EventAuthValidation {
  override def rejectionCause: String = "InvalidStateKeyContent: The state key start with '@' and doesn't match sender"
}

class EventValidator(implicit runtimeContext: RuntimeContext) {
  protected[this] val authRepo        = new AuthRepo
  protected[this] val eventRepo       = new EventRepo
  protected[this] val stateSetService = runtimeContext.newStateSetService

  protected def authChain(event: Event): Try[Set[Event]] =
    for {
      authEvents <- eventRepo.getAuthEventsWithCache(event.eventId)
      r <- Try {
        authEvents.flatMap(e => authChain(e).get)
      }
    } yield authEvents.union(r)

  protected def parentChain(event: Event): Try[Set[Event]] =
    for {
      parentsEvents <- eventRepo.getParentEventsWithCache(event.eventId)
      r <- Try {
        parentsEvents.flatMap(e => parentChain(e).get)
      }
    } yield parentsEvents.union(r)

  /**
    * Reject if event has auth_events that:
    *  - have duplicate entries for a given type and state_key pair
    *  - have entries whose type and state_key don't match those specified by the auth events selection algorithm
    *  described in the server specification.
    *
    * If event does not have a m.room.create in its auth_events, reject.
    *
    * @param e
    * @param stateSet
    * @return
    */
  protected def validateAuthEvents(e: Event, stateSet: StateSet): EitherT[Future, EventAuthValidation, Event] =
    EitherT {
      Future
        .fromTry(authChain(e))
        .map { authEvents =>
          val duplicates = authEvents.groupBy(event => (event.eventType, event.stateKey.get)).count(_._2.size > 1)
          if (duplicates > 0)
            Left(DuplicateEntriesInAuthEvents(e))
          else {
            val otherAuthTypes = authEvents
              .map(_.eventType)
              .diff(
                Set(
                  EventTypes.M_ROOM_CREATE,
                  EventTypes.M_ROOM_MEMBER,
                  EventTypes.M_ROOM_POWER_LEVELS,
                  EventTypes.M_ROOM_JOIN_RULES
                )
              )
            if (otherAuthTypes.isEmpty)
              Right(e)
            else
              Left(InvalidEventTypeInAuthChain(e, otherAuthTypes.toString()))
          }
        }
        .recover(f => Left(InternalError(e, s"Couldn't get event auth chain: ${f.getMessage}")))
    }

  protected def validateSenderHasJoined(event: Event, stateSet: StateSet): EitherT[Future, EventAuthValidation, Event] =
    EitherT {
      val memberShipFuture = for {
        sender     <- Future.fromTry(authRepo.getUserByIdWithCache(event.senderId))
        memberShip <- Future.fromTry(stateSetService.getMembershipEventContent(sender.get.mxUserId, stateSet))
      } yield (sender, memberShip)
      memberShipFuture
        .map {
          case (Some(_), Some(membershipContent)) if membershipContent.membership == "join" => Right(event)
          case (Some(sender), _)                                                            => Left(IncompatibleMembership(event, sender.mxUserId, "!join"))
          case other                                                                        => Left(InternalError(event, s"Unexpected result from state set: $other"))
        }
        .recover(f => Left(InternalError(event, s"Couldn't validate user joined membership: ${f.getMessage}")))
    }

  protected def validateRequiredPowerLevel(
      event: Event,
      stateSet: StateSet
  ): EitherT[Future, EventAuthValidation, Event] =
    EitherT {
      val f1 = Future.fromTry(stateSetService.getPowerLevelEventContent(stateSet))
      val plFuture = for {
        sender <- Future.fromTry(authRepo.getUserByIdWithCache(event.senderId))
        userPl <- Future.fromTry(stateSetService.userPowerLevel(sender.get.mxUserId, stateSet))
        pl     <- f1
      } yield (sender, userPl, pl)
      plFuture
        .map {
          case (Some(sender), Some(userPl), Some(pl)) =>
            val requiredPl = PowerLevelsUtils.requiredPowerLevelForEventType(event.eventType, pl)
            if (requiredPl > userPl)
              Left(
                InsufficientPowerLevels(event, sender.mxUserId, s" event type ${event.eventType}", userPl, requiredPl)
              )
            else Right(event)
          case (Some(_), None, None) => Right(event) //No power level in room
          case other                 => Left(InternalError(event, s"Unexpected result from state set: $other"))
        }
        .recover(f => Left(InternalError(event, s"Couldn't validate required power level: ${f.getMessage}")))
    }

  protected def validateStateKeyFormat(event: Event): EitherT[Future, EventAuthValidation, Event] =
    EitherT {
      Future
        .fromTry(authRepo.getUserByIdWithCache(event.senderId))
        .map {
          case Some(sender) =>
            if (event.stateKey.get.startsWith("@") && event.stateKey.get != sender.mxUserId)
              Left(InvalidStateKeyContent(event))
            else Right(event)
          case other => Left(InternalError(event, s"Unexpected result from state set: $other"))
        }
        .recover(f => Left(InternalError(event, s"Couldn't validate state key format: ${f.getMessage}")))
    }

  /**
    * Default validation rules
    * @param event
    * @param stateSet
    * @return
    */
  def validateDefault(event: Event, stateSet: StateSet): EitherT[Future, EventAuthValidation, Event] = {
    val f1 = validateAuthEvents(event, stateSet)
    val f2 = validateSenderHasJoined(event, stateSet)
    val f3 = validateRequiredPowerLevel(event, stateSet)
    val f4 = validateStateKeyFormat(event)

    for {
      _ <- f1
      _ <- f2
      _ <- f3
      _ <- f4
    } yield event
  }

}
