package org.mascarene.homeserver.internal.federation

import akka.actor.typed.{ActorSystem, Behavior}
import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors}
import akka.cluster.sharding.typed.scaladsl.EntityTypeKey
import com.typesafe.scalalogging.LazyLogging
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters
import org.bouncycastle.util.io.pem.{PemHeader, PemReader}
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.services.FederationService
import org.mascarene.matrix.server.KeySpec
import org.mascarene.matrix.server.r0.api.FederationClient
import org.mascarene.utils.Codecs

import java.io.FileReader
import scala.jdk.DurationConverters._
import scala.util.Try
import scala.jdk.CollectionConverters._

object FederationAgent {
  val TypeKey: EntityTypeKey[FederationAgentCommand] = EntityTypeKey[FederationAgentCommand]("FederationAgent")

  def apply(
      entityId: String,
      runtimeContext: RuntimeContext
  ): Behavior[FederationAgentCommand] =
    Behaviors.setup { ctx => new FederationAgent(entityId, ctx, runtimeContext) }
}
class FederationAgent(
    authority: String,
    context: ActorContext[FederationAgentCommand],
    implicit val runtimeContext: RuntimeContext
) extends AbstractBehavior[FederationAgentCommand](context)
    with LazyLogging
    with ImplicitAskTimeOut {
  implicit val system: ActorSystem[Nothing] = context.system
  implicit val ec                           = system.executionContext

  private val authorityNameCacheTime =
    runtimeContext.config.getDuration("mascarene.server.federation.server-resolve-cache-time")
  private val privateKeyFile = runtimeContext.config.getString("mascarene.server.federation.server-key-file")

  private val keySpec          = loadKeySpec(privateKeyFile).get //Exception may be thrown here
  private[this] var agentState = FederationAgentState(keySpec.keyId, None)

  private val federationService: FederationService = runtimeContext.newFederationService
  federationService
    .storeEd25529LocalKey(keySpec.keyId, keySpec.publicKey.getEncoded)
    .map(serverKey => context.self ! InitialState(agentState.copy(serverKey = Some(serverKey))))
  val federationClient = new FederationClient(keySpec, authority, authorityNameCacheTime.toScala)

  context.log.debug(s"federation agent for authority $authority init completed")

  override def onMessage(msg: FederationAgentCommand): Behavior[FederationAgentCommand] = {
    msg match {
      case InitialState(state) =>
        agentState = state
        Behaviors.same
    }
  }

  private def loadKeySpec(fileName: String): Try[KeySpec] =
    Try {
      val pemReader = new PemReader(new FileReader(fileName))
      val pemObject = pemReader.readPemObject()
      pemObject.getHeaders.asScala.map(_.asInstanceOf[PemHeader]).find(_.getName == "keyId") match {
        case None => throw new IllegalArgumentException(s"keyId header not found in $fileName PEM file")
        case Some(keyId) =>
          val privateKey = new Ed25519PrivateKeyParameters(pemObject.getContent, 0)
          val publicKey  = privateKey.generatePublicKey()
          logger.whenInfoEnabled {
            val encodedPublicKey = Codecs.toBase64(publicKey.getEncoded, unpadded = true)
            logger.info(s"Loaded keyId=ed25519:${keyId.getValue} key=$encodedPublicKey")
          }
          KeySpec(keyId.getValue, publicKey, privateKey)
      }
    }
}
