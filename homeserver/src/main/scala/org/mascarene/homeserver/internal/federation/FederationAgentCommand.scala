package org.mascarene.homeserver.internal.federation

sealed trait FederationAgentCommand
case class InitialState(state: FederationAgentState) extends FederationAgentCommand
case class GetPublicRooms(
    limit: Option[Integer] = None,
    since: Option[String] = None,
    includeAllNetworks: Option[Boolean],
    third_party_instance_id: Option[String] = None
) extends FederationAgentCommand
