/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.rooms.validators

import com.typesafe.scalalogging.LazyLogging
import cats.data.EitherT
import cats.implicits._
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.model.{Event, StateSet}

import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.Future

class MRoomThirdPartyInviteEventValidator(implicit runtimeContext: RuntimeContext)
    extends EventValidator
    with LazyLogging {

  def validate(event: Event, stateSet: StateSet): EitherT[Future, EventAuthValidation, Event] = {
    val f1 = validateAuthEvents(event, stateSet)
    val f2 = validateSenderHasJoined(event, stateSet)
    val f3 = EitherT[Future, EventAuthValidation, Event] {
      val fpl = Future.fromTry(stateSetService.getPowerLevelEventContent(stateSet))
      val futurePl = for {
        sender <- Future.fromTry(authRepo.getUserByIdWithCache(event.senderId))
        userPl <- Future.fromTry(stateSetService.userPowerLevel(sender.get.mxUserId, stateSet))
        pl     <- fpl
      } yield (sender, userPl, pl)
      futurePl
        .map {
          case (Some(sender), Some(userPl), Some(statePl)) =>
            if (userPl >= statePl.invite)
              Right(event)
            else
              Left(InsufficientPowerLevels(event, sender.mxUserId, "invite", userPl, statePl.invite))
          case _ => Left(InternalError(event, "Couldn't get/decode sender or target power level events"))
        }
        .recover(f => Left(InternalError(event, s"Couldn't validate third party event: ${f.getMessage}")))
    }
    for {
      _ <- f1
      _ <- f2
      _ <- f3
    } yield event
  }
}
