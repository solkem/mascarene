/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.rooms.validators

import org.mascarene.homeserver.internal.repository.{AuthRepo, EventRepo}
import org.mascarene.utils.{RoomIdentifierUtils, UserIdentifierUtils}
import cats.data.EitherT
import cats.implicits._
import org.mascarene.homeserver.internal.model.{Event, Room}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

case class MRoomCreateHasNoParent(event: Event, parents: Set[Event]) extends EventAuthValidation {
  override def rejectionCause: String = s"MRoomCreateHasNoParent: event has one or more previous event: $parents"
}

class MRoomCreateEventValidator(room: Room, eventRepo: EventRepo, authRepo: AuthRepo) {
  private def validateMRoomCreateEventHasNoParent(event: Event): EitherT[Future, EventAuthValidation, Event] =
    EitherT {
      Future
        .fromTry(eventRepo.getParentEventsWithCache(event.eventId))
        .map { parents =>
          if (parents.nonEmpty)
            Left(MRoomCreateHasNoParent(event, parents))
          else
            Right(event)
        }
        .recover(f => Left(InternalError(event, s"Couldn't not get event's parents: ${f.getMessage}")))
    }

  private def validateRoomIdDomainMatchesSenderDomain(event: Event): EitherT[Future, EventAuthValidation, Event] =
    EitherT {
      Future {
        val domainMatch = for {
          sender         <- authRepo.getUserByIdWithCache(event.senderId)
          userIdentifier <- UserIdentifierUtils.parse(sender.get.mxUserId)
          roomIdentifier <- RoomIdentifierUtils.parse(room.mxRoomId)
        } yield userIdentifier.domain == roomIdentifier.domain
        domainMatch match {
          case Failure(f)     => Left(InvalidIdentifierFormat(f.getMessage, event))
          case Success(false) => Left(RoomDomainMismatchSenderDomain(event))
          case Success(true)  => Right(event)
        }
      }
    }

  def validate(event: Event): EitherT[Future, EventAuthValidation, Event] = {
    val f1 = validateMRoomCreateEventHasNoParent(event)
    val f2 = validateRoomIdDomainMatchesSenderDomain(event)
    for {
      _ <- f1
      _ <- f2
    } yield event
  }
}
