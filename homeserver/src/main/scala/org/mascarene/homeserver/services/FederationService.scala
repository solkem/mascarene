package org.mascarene.homeserver.services

import akka.actor.typed.scaladsl.AskPattern._
import akka.cluster.sharding.typed.scaladsl.EntityRef
import com.typesafe.scalalogging.LazyLogging
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.internal.federation.{
  FederationAgentCommand,
  FederationClusterCommand,
  GetFederationAgent,
  GotFederationAgent
}
import org.mascarene.homeserver.internal.model.federation.ServerKey
import org.mascarene.homeserver.internal.repository.federation.ServerKeyRepo

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class FederationService(implicit val runtimeContext: RuntimeContext) extends ImplicitAskTimeOut with LazyLogging {
  implicit val sys = runtimeContext.actorSystem

  private val matrixServerName    = runtimeContext.config.getString("mascarene.server.domain-name")
  private[this] val serverKeyRepo = new ServerKeyRepo
  private val federationCluster   = runtimeContext.actorRegistry("FederationCluster").narrow[FederationClusterCommand]

  private def getFederationAgent(forAuthority: String): Future[EntityRef[FederationAgentCommand]] =
    federationCluster.ask[GotFederationAgent](GetFederationAgent(forAuthority, _)).map(_.serverRef)

  def storeEd25529LocalKey(keyId: String, publicKey: Array[Byte]): Future[ServerKey] =
    Future.fromTry {
      serverKeyRepo.storeServerKey(matrixServerName, keyId, publicKey)
    }

  def expiresLocalKeys(): Unit = ???
}
