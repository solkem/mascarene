TWIM:

# Mascarene

[Mascarene v0.2.0](https://gitlab.com/mascarene/mascarene/-/releases/v0.2.0) has been released. 

This version implements a minimal set of client API endpoints to work with Riot and allow user registration, 
room creation, invitation and messages sending on the same running instance (no federation support).

[Docker image](https://hub.docker.com/r/mascarene/mascarene-homeserver) are also available. See [this documentation](https://gitlab.com/mascarene/mascarene/-/blob/master/docs/install.md)
for rough installation instructions.

Join us at [#mascarene:beerfactory.org](https://matrix.to/#/#mascarene:beerfactory.org) 
