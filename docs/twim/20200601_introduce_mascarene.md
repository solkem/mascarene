TWIM:

# Mascarene

[Mascarene](https://gitlab.com/mascarene/mascarene) is a quite new homeserver implementation project started a few months ago. Now it's time to introduce it.

Mascarene is written in [Scala](https://www.scala-lang.org/), runs on JVM and relies on [Akka](https://akka.io/) actor model. Out of the box it provides features like 
efficient streaming I/O, clustering and live data distribution. Data are backed in a PostgreSQL database.

The project is in early stage but already provides a few endpoints which make Mascarene able to talk to Riot. 
You can test it at https://snapshot.mascarene.org. You should be able to register, login, create a room and talk to yourself.
   
Current work are focused on client API implementation; federation or e2ee will come later. 

Contributions are welcome, join us on #mascarene:beerfactory.org

